﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaskHealth : HealthScript {
	
	[SerializeField] public GameObject eyeLeft;
	[SerializeField] GameObject eyeRight;
	[SerializeField] new Rigidbody rigidobdy;
	[SerializeField] EnemyScript enemyScript;
	[SerializeField] Material material;
	[SerializeField] ParticleSystem hitParticle;

	Vector3 defaultLocalPosition;
	Quaternion defaultLocalRotation;

    // Use this for initialization
    void Awake () 
	{
        Init();
		defaultLocalPosition = transform.localPosition;
		defaultLocalRotation = transform.localRotation;
	}

	public override void Death (Vector3 hitPosition)
    {
        gameObject.transform.parent = null;
		rigidobdy.isKinematic = false;
		rigidobdy.useGravity = true;
		eyeLeft.SetActive (false);
		eyeRight.SetActive (false);

		StartCoroutine (disappearTimer ());
    }

	public void Reset (Transform parent)
	{
		transform.SetParent (parent);
		eyeLeft.SetActive (true);
		eyeRight.SetActive (true);
		rigidobdy.isKinematic = true;
		rigidobdy.useGravity = false;

		transform.localPosition = defaultLocalPosition;
		transform.localRotation = defaultLocalRotation;

		BoxCollider [] colliders = GetComponents <BoxCollider> ();

		for (int i = 0; i < colliders.Length; i ++)
		{
			colliders [i].isTrigger = false;
		}

		dead = false;
		currentHealth = MaxHealth;
	}

	IEnumerator disappearTimer ()
	{
		yield return new WaitForSeconds (GlobalConst.ENEMY_DISAPPEAR_TIME);

		disappear ();
	}

	void disappear ()
	{
		BoxCollider [] colliders = GetComponents <BoxCollider> ();

		for (int i = 0; i < colliders.Length; i ++)
		{
			colliders [i].isTrigger = true;
		}
	}

	public override bool IsActive ()
	{
        return enemyScript.IsActive ();
	}

	public override void DealDamage (int damage, Vector3 hitPosition)
	{
		if (! dead)
		{
			onHit (hitPosition);
		}

		base.DealDamage (damage, hitPosition);
	}

	void onHit (Vector3 hitPosition)
	{
		if (hitPosition != GlobalConst.INVALID_VECTOR)
		{
			hitParticle.transform.position = hitPosition;
			hitParticle.Play ();
			blinkOnHit ();
		}
	}
}
