﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class ThrowingScript : MonoBehaviour, Controls {

	public const float BLINK_COST = 0.33f;
	public const int KATANA_PICK_UP_DISTANCE = 10;

	[SerializeField] Transform katanaTransform;
	[SerializeField] ThrowThisScript katanaScript;
	[SerializeField] MeshRenderer katanaMesh;
	[SerializeField] bool active = true;

	[SerializeField] Image blinkSlider;

    private RigAnimationManager animManager;
    private BlinkingScript blinkingScript;
    private FPSMovementScript fpsMovementScript;

    private bool throwing = false;

	public bool Active
	{
		get { return active; }
		set { active = value; }
	}

    public bool IsThrown()
    {
        return katanaScript.getIsThrown();
    }

    public void PickUp ()
    {
        katanaScript.PickUp ();
        throwing = false;
        katanaMesh.enabled = true;
        animManager.Normal();
		Debug.Log ("Pick Up katana");
    }

    public void Throw()
    {
        katanaMesh.enabled = false;
        katanaScript.ThrowThis();
        throwing = false;
    }

    public bool isThrowing()
    {
        return throwing;
    }

    // Use this for initialization
    void Start () 
	{
        animManager = GetComponent<RigAnimationManager>();
        blinkingScript = GetComponent<BlinkingScript>();
        fpsMovementScript = GetComponent<FPSMovementScript>();
	}
	
	// Update is called once per frame
	void Update () {
		
		if (active) 
		{
			if (Input.GetKeyDown(KeyCode.LeftShift) && ! katanaScript.getIsThrown() &&
				! throwing &&
				animManager.GetState() != RigAnimationManager.State.ATTACK &&
				animManager.GetState() != RigAnimationManager.State.COUNTER)
			{
				throwing = true;
				animManager.Throw();
			}
			if (Input.GetKeyDown(KeyCode.LeftShift) && katanaScript.getIsThrown())
			{
				if (blinkSlider.fillAmount < BLINK_COST)
				{
					blinkSlider.GetComponentInParent<BlinkImage>().BlinkRed();
					return;
				}

				blinkSlider.fillAmount -= BLINK_COST;
				float d = Vector3.Distance (katanaTransform.position, transform.position);

				if (d < KATANA_PICK_UP_DISTANCE)
				{
					PickUp();
				}
				else
				{
					blinkingScript.BlinkToKatana(katanaTransform);
					katanaScript.Freeze();
				}
			}
		}
    }

    void OnTriggerEnter(Collider other)
    {
		if (other.gameObject.tag == GlobalConst.KATANA_TAG && IsThrown ()) 
		{
			PickUp ();
		}
    }

	void OnCollisionEnter (Collision collision)
	{
		if (collision.collider.tag == GlobalConst.KATANA_TAG && IsThrown ())
		{
			PickUp ();
		}
	}

    public bool IsActive()
    {
        return active;
    }

    public void SetActive(bool active)
    {
        this.active = active;
    }
}
