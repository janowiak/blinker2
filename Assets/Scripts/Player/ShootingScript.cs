﻿using UnityEngine;
using System.Collections;
using System;

public class ShootingScript : MonoBehaviour, Controls {

	public delegate void ShootBulletHandler(Bullet.ShootType shootType, Bullet.BulletType type, Transform transform, Vector3 speed);
    public event ShootBulletHandler ShootBullet;

	[SerializeField] ParticleSystem shootParticles;
	[SerializeField] GameObject bullet;
	[SerializeField] Transform spawnPoint;
	[SerializeField] new Camera camera;
	[SerializeField] bool active = true;

    private RigAnimationManager animManager;
    private CounteringScript counteringScript;
    private float shootTimer = 0;
	private Vector3 prevPosition;
	private Vector3 speed = Vector3.zero;
	Bullet.ShootType shootType = Bullet.ShootType.NORMAL;
	float fireRate = Bullet.DEFAULT_FIRE_RATE;

	public float FireRate
	{
		get { return fireRate; }
		set { fireRate = value; }
	}

	public bool Active
	{
		get { return active; }
		set { active = value; }
	}

	public Bullet.ShootType ShootType
	{
		get { return shootType; }

		set 
		{
			shootType = value; 
			UnityEngine.ParticleSystem.MinMaxGradient startColor = shootParticles.main.startColor;

			switch (shootType)
			{
				case Bullet.ShootType.NORMAL:
					startColor = new ParticleSystem.MinMaxGradient (Bullet.PLAYER_NORMAL_BULLET_COLOR);

					break;

				case Bullet.ShootType.AUTO_AIM:
					startColor= new ParticleSystem.MinMaxGradient (Bullet.PLAYER_AUTO_AIM_BULLET_COLOR);

					break;

				case Bullet.ShootType.SPREAD:
					startColor = new ParticleSystem.MinMaxGradient (Bullet.PLAYER_SPREAD_BULLET_COLOR);

					break;
			}
		}
	}

    // Use this for initialization
    void Start ()
	{
        animManager = GetComponent<RigAnimationManager>();
        counteringScript = GetComponent<CounteringScript>();
		prevPosition = transform.position;

		ShootType = Bullet.ShootType.NORMAL;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (active)
		{
			//Change bulletSpawn rotation to shoot bullets where gunsight aims
			RaycastHit hit;
			Vector3 rayDirection = camera.transform.forward;
			Ray ray = new Ray(camera.transform.position + rayDirection * 5, rayDirection);

			if (Physics.Raycast(ray, out hit))
			{
				Debug.DrawLine(camera.transform.position, hit.point);
				Vector3 shootDirection = hit.point - spawnPoint.position;
				spawnPoint.rotation = Quaternion.LookRotation(shootDirection);
			}
			else
			{
				Vector3 shootDirection = camera.transform.position + rayDirection * 100000 - spawnPoint.position;
				spawnPoint.rotation = Quaternion.LookRotation(shootDirection);
			}

			shootTimer += Time.deltaTime;

			if (Input.GetKey (KeyManager.Instance.GetKeyForGameAction (KeyManager.GameAction.SHOOT)) && ! counteringScript.IsCountering() && shootTimer > fireRate)
			{
				if (ShootBullet != null)
				{  
					shootTimer = 0;
					animManager.Shoot();
					ShootBullet (shootType, Bullet.BulletType.PLAYER, spawnPoint, speed);
					shootParticles.Play ();
				}
			}
		}
        
    }

	void FixedUpdate ()
	{
		calculateSpeed ();
	}

	void calculateSpeed ()
	{
		Vector3 posDiff = transform.position - prevPosition;
		prevPosition = transform.position;
		speed = posDiff / Time.deltaTime;
	}

    public float getShootTimer()
    {
        return shootTimer;
    }

    public bool IsActive()
    {
        return active;
    }

    public void SetActive(bool active)
    {
        this.active = active;
    }
}
