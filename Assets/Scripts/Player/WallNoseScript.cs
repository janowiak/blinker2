﻿using UnityEngine;
using System.Collections;

//Starts animation when nose hits obstacle
public class WallNoseScript : MonoBehaviour {

    public RigAnimationManager animManager;

    void OnTriggerEnter(Collider other)
    {
		if (other.gameObject.tag == GlobalConst.WALL_TAG || other.gameObject.tag == GlobalConst.ENVIRONMENT_TAG)
		{
			animManager.WallOn();
		}   
    }

    void OnTriggerExit(Collider other)
    {
		if (other.gameObject.tag == GlobalConst.WALL_TAG || other.gameObject.tag == GlobalConst.ENVIRONMENT_TAG)
		{
			animManager.WallOf();
		}       
    }
}
