﻿using UnityEngine;
using System.Collections;

public class BladeCollisionScript : MonoBehaviour {

	[SerializeField] Rigidbody rb;
	[SerializeField] ThrowThisScript throwKatanaScript;

    private bool collided = false;
    private bool stuck = false;

    public bool IsStuck()
    {
        return stuck;
    }

    public void ResetStuck()
    {
        stuck = false;
    }

    void OnTriggerEnter (Collider other)
    {
		if (other.tag == GlobalConst.WALL_TAG && throwKatanaScript.GetCanStuck ())
        {
            stuck = true;
            float magnitude = rb.velocity.magnitude;

            if (magnitude >= 6)
            {
                rb.velocity = Vector3.zero;
                rb.angularVelocity = Vector3.zero;
                rb.isKinematic = true;
            }
        }
    }
}
