﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SwitchAction : MonoBehaviour {

	public abstract void Action ();
	public abstract string GetKeyForAction ();
	public abstract string GetActionName ();
}
