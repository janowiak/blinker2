﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageSwitchAction : SwitchAction {

	[SerializeField] GameObject environment;
	[SerializeField] ShootingScript shootingScript;
	[SerializeField] CounteringScript counteringScript;
	[SerializeField] BlinkingScript blinkingScript;
	[SerializeField] ThrowingScript throwingScript;
	[SerializeField] GameObject rig;

	[SerializeField] GameplayController gameplayController;

	public override void Action ()
	{
		environment.SetActive (false);

		shootingScript.Active = true;
		counteringScript.Active = true;
		blinkingScript.Active = true;
		throwingScript.Active = true;

		rig.SetActive (true);
	}

	public override string GetActionName ()
	{
		return "PICK UP";
	}

	public override string GetKeyForAction ()
	{
		return KeyManager.Instance.GetKeyForGameAction (KeyManager.GameAction.INTERACTION).ToString ();
	}
}
