﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EnemySpawner : MonoBehaviour {

	[SerializeField] GameObject enemiesPool;
	[SerializeField] GameObject enemyPrefab;
	[SerializeField] int maxEnemies;

	List <EnemyScript> enemies;
	float spawnEnemyTime = 3f;

	public const float SPAWN_TIME_DECREASE_SPEED = 0.3f;
	public const float MIN_SPAWN_ENEMY_TIME = 1f;
	public const float SPAWN_RANGE_X = 200f;
	public const float SPAWN_RANGE_Y = 100f;
	public const float SPAWN_RANGE_Z = 200f;

	static System.Random rand = new System.Random ();

	public Transform GetClosestEnemy (Vector3 position)
	{
		float minD = int.MaxValue;
		Transform closestEnemy = null;
		float d = 0;

		for (int i = 0; i < enemies.Count; i++)
		{
			if (enemies [i].IsActive ())
			{
				d = Vector3.Distance (enemies [i].gameObject.transform.position, position);

				if (d < minD)
				{
					minD = d;
					closestEnemy = enemies [i].gameObject.transform;
				}
			}
		}

		return closestEnemy;
	}

    public List<Tuple <Transform, float>> GetNColosestEnemies(Vector3 position, int n)
    {
        List<Transform> enemiesTransforms = new List<Transform> ();
        List<Tuple<Transform, float>> result;

        for (int i = 0; i < enemies.Count; i++)
        {
            if (enemies [i].IsActive ())
            {
                enemiesTransforms.Add (enemies [i].transform);
            }
        }

        result = AIHelper.GetNClosest (enemiesTransforms, n, position);

        return result;
    }


    public void StartSpawningEnemies ()
	{
		StopSpawningEnemies ();
		StartCoroutine (spawnEnemyCoroutine ());
	}

	public void StopSpawningEnemies ()
	{
		StopCoroutine (spawnEnemyCoroutine ());
	}

	IEnumerator spawnEnemyCoroutine ()
	{
		while (true)
		{
			yield return new WaitForSeconds (spawnEnemyTime);

			bool result = spawnEnemy ();

			if (result)
			{
				spawnEnemyTime -= SPAWN_TIME_DECREASE_SPEED;
			}

			if (spawnEnemyTime < MIN_SPAWN_ENEMY_TIME)
			{
				spawnEnemyTime = MIN_SPAWN_ENEMY_TIME;
			}
		}
	}

	bool spawnEnemy ()
	{
		bool result = false;
		int pointer = GlobalConst.INVALID_VALUE;

		for (int i = 0; i < enemies.Count; i++ )
		{
			if (! enemies [i].gameObject.activeSelf)
			{
				pointer = i;

				break;
			}
		}

		if (pointer != GlobalConst.INVALID_VALUE)
		{
			result = true;

			float x = (float) (rand.NextDouble () * SPAWN_RANGE_X);
			float y = (float) (rand.NextDouble () * SPAWN_RANGE_Y);
			float z = (float) (rand.NextDouble () * SPAWN_RANGE_Z);

			enemies [pointer].Spawn (new Vector3 (x, y, z) + enemiesPool.transform.position);
		}

		return result;
	}

	public void InitEnemies (EnemyScript.ShootBulletHandler ShootBullet, EnemyScript.OnDeathEventHandler OnEnemyDeath, EnemyScript.OnDamageDealtEventHandler OnDamageToEnemyDealt)
	{
		enemies = new List <EnemyScript> ();

		for (int i = 0; i < maxEnemies; i++)
		{
			GameObject newEnemy = Instantiate (enemyPrefab);
			newEnemy.SetActive (false);
			newEnemy.transform.SetParent (enemiesPool.transform);
			EnemyScript enemyScript = newEnemy.GetComponent <EnemyScript> ();
			enemyScript.ShootBullet += ShootBullet;
			enemyScript.OnDeath += OnEnemyDeath;
			enemyScript.OnDamageDealt += OnDamageToEnemyDealt;

			enemies.Add (enemyScript);

            enemyScript.SetActive (false);
		}
	}
}
