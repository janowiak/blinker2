﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : HealthScript
{
	[SerializeField] Image healthSlider;
	[SerializeField] Image bloodOverlay;

    // Use this for initialization
    void Start () 
	{
        Init();
    }

	public int CurrentHealth
	{
		get { return currentHealth; }
	}

	public override void DealDamage(int damage, Vector3 hitPosition)
    {
		base.DealDamage (damage, hitPosition);

		healthSlider.fillAmount = (currentHealth / (float) MaxHealth);

		if (damage > 0) 
		{
			healthSlider.GetComponent<BlinkImage> ().BlinkRed ();
			bloodOverlay.GetComponent<BloodOverlayScript> ().Blink ();
		}
		else
		{
			healthSlider.GetComponent<BlinkImage> ().BlinkRed ();
			bloodOverlay.GetComponent<BloodOverlayScript> ().BlinkGreen ();
		}
        
    }

	public override void Death (Vector3 hitPosition)
    {
		//TODO
    }

	public override bool IsActive ()
	{
		return true;
	}
}
