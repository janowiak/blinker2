﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageProcessing : MonoBehaviour {

	public static void ChangeImageAlpha (Image image, float alpha)
	{
		Color color = image.color;
		color.a = alpha;
		image.color = color;
	}

	public static void ChangeTextAlpha (Text text, float alpha)
	{
		Color color = text.color;
		color.a = alpha;
		text.color = color;
	}

	public static void ChangeTextMeshAlpha (TextMesh text, float alpha)
	{
		Color color = text.color;
		color.a = alpha;
		text.color = color;
	}

	public static void ChangeSpriteRendererAlpha (SpriteRenderer image, float alpha)
	{
		Color color = image.color;
		color.a = alpha;
		image.color = color;
	}

	public static void ChangeUIGroupAlpha (GameObject parent, float alpha)
	{
		Image [] images = parent.GetComponentsInChildren <Image> ();
		Text [] texts = parent.GetComponentsInChildren <Text> ();

		if (images != null)
		{
			for (int i = 0; i < images.Length; i++)
			{
				ChangeImageAlpha (images [i], alpha);
			}
		}

		if (texts != null)
		{
			for (int i = 0; i < texts.Length; i++)
			{
				ChangeTextAlpha (texts [i], alpha);
			}
		}
	}
}
