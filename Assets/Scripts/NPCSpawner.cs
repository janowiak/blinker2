﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCSpawner : MonoBehaviour {

	[SerializeField] GameObject pointsOfInterestContainer;
	[SerializeField] GameObject npc;
	[SerializeField] int amount = 10;

	// Use this for initialization
	void Start () 
	{
		spawnNPCs ();

        
    }

	void spawnNPCs ()
	{
		if (pointsOfInterestContainer != null)
		{
			int childCount = pointsOfInterestContainer.transform.childCount;

			int npcCount = 0;

			while (npcCount < amount)
			{
				for (int i = 0; i < childCount; i++)
				{
					GameObject npcInstance = Instantiate(npc);
					npcInstance.transform.position = pointsOfInterestContainer.transform.GetChild(i).transform.position;
					npcCount++;

					if (npcCount >= amount)
					{
						break;
					} 
				}
			}
		}
	}
}
