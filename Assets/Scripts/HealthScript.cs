﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class HealthScript : MonoBehaviour
{
	[SerializeField] List <Renderer> renderers;
	[SerializeField] int maxHealth = 100;

    protected int currentHealth = 100;
    protected bool dead = false;

	public delegate void OnDeathEventHandler (Vector3 hitPosition);
	public event OnDeathEventHandler OnDeath;
	public delegate void OnDamageDealtEventHandler (Vector3 hitPosition);
	public event OnDamageDealtEventHandler OnDamageDealt;

	public const float BLINK_ON_HIT_ANIMATION_DURATION = 0.05f;
	public static Color BLINK_ON_HIT_COLOR = Color.red;

	public int MaxHealth
	{
		get { return maxHealth; }
	}

    public bool Dead
    {
        get { return dead; }
    }

    void Start()
    {
        Init();
    }

    protected void Init()
    {
        currentHealth = maxHealth;
    }

	public virtual void DealDamage (int damage, Vector3 hitPosition)
    {
		if (IsActive ())
		{
			currentHealth -= damage;

			if (OnDamageDealt != null)
			{
				OnDamageDealt (hitPosition);
			}

			if (currentHealth <= 0)
			{
				if (! dead)
				{
					Death (hitPosition);
					dead = true;
				}
			}
		}
    }

	public virtual void Death (Vector3 hitPosition)
	{
		if (OnDeath != null)
		{
			OnDeath (hitPosition);
		}
	}
	 
	public abstract bool IsActive ();

	protected void blinkOnHit ()
	{
		StopCoroutine (playBlinkAnimation ());
		StartCoroutine (playBlinkAnimation ());
	}

	IEnumerator playBlinkAnimation ()
	{
		for (int i = 0; i < renderers.Count; i ++)
		{
			Material material = renderers [i].material;
			material.color = BLINK_ON_HIT_COLOR;
		}

		yield return new WaitForSeconds (BLINK_ON_HIT_ANIMATION_DURATION);

		for (int i = 0; i < renderers.Count; i ++)
		{
			Material material = renderers [i].material;
			material.color = Color.white;
		}
	}
}
