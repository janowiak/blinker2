﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : Activable {

	public const float ENEMY_SHOOT_CHANCE = 0.9f;

	public delegate void ShootBulletHandler(Bullet.ShootType shootType, Bullet.BulletType type, Transform transform, Vector3 speed);
    public event ShootBulletHandler ShootBullet;

	public delegate void OnDeathEventHandler ();
	public event OnDeathEventHandler OnDeath;

	public delegate void OnDamageDealtEventHandler ();
	public event OnDamageDealtEventHandler OnDamageDealt;

	[SerializeField] GameObject spawnCilinder;
	[SerializeField] new Rigidbody rigidbody;
    [SerializeField] Transform bulletSpawnLeft, bulletSpawnRight;
    [SerializeField] GameObject projectile;
    [SerializeField] float rotationSpeed = 1f;
	[SerializeField] EnemyHealthScript healthScript;
    [SerializeField] float movementSpeed = 1f;
	[SerializeField] Transform model;
	[SerializeField] EnemyHealthScript enemyHealth;
	[SerializeField] ParticleSystem hitParticle;
	[SerializeField] ParticleSystem deathParticle;

    Vector3 offsetVector;
    bool active = false;
    bool shootSide = false;
    Vector3 lastPlayerPosition;
    bool canSeePlayer = true;
	Transform player;

    private static System.Random rand = new System.Random();

	static Vector3 spawnCilinderMinScale = new Vector3 (9f, 0.01f, 9f);
	static Vector3 spawnCilinderDefaultScale = new Vector3 (9f, 6f, 9f);
	const float spawnAnimationDuration = 1f;

	public const float MIN_DISTANCE_TO_PLAYER = 10f;
	public const float DISTANCE_TO_PLAYER_RANGE = 10f;

    public override bool IsActive ()
    {
        return active;
    }

    public override void SetActive (bool active)
    {
        this.active = active;
    }

	void Start () 
	{
		player = GameObject.FindGameObjectWithTag (GlobalConst.PLAYER_TAG).transform;
		offsetVector = new Vector3 ((float) (MIN_DISTANCE_TO_PLAYER + DISTANCE_TO_PLAYER_RANGE * rand.NextDouble()),
			(float) (MIN_DISTANCE_TO_PLAYER + DISTANCE_TO_PLAYER_RANGE * rand.NextDouble()),
			(float) (MIN_DISTANCE_TO_PLAYER + DISTANCE_TO_PLAYER_RANGE * rand.NextDouble()));
        lastPlayerPosition = player.position;
		healthScript.OnDeath += HealthScript_OnDeath;
		healthScript.OnDamageDealt += HealthScript_OnDamageDealt;

		Disable ();
	}

	void HealthScript_OnDamageDealt (Vector3 hitPosition)
	{
		if (OnDamageDealt != null)
		{
			OnDamageDealt ();
		}

		if (hitPosition != GlobalConst.INVALID_VECTOR)
		{
			hitParticle.transform.position = hitPosition;
			hitParticle.Play ();
		}
	}

	void HealthScript_OnDeath (Vector3 hitPosition)
	{
		if (OnDeath != null)
		{
			OnDeath ();

			if (hitPosition != GlobalConst.INVALID_VECTOR)
			{
				deathParticle.transform.position = hitPosition;
				deathParticle.Play ();
			}
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
        if (active)
        {
            findLastPlayerPosition();
            Turn();
            follow();            
        }
	}

    void findLastPlayerPosition ()
    {
        Vector3 raycastDirection = player.position - transform.position;
        RaycastHit hit;

        if (Physics.Raycast(transform.position + transform.forward * 5, raycastDirection, out hit))
        {
			if (string.Equals (hit.collider.tag, GlobalConst.PLAYER_TAG) || string.Equals (hit.collider.tag, GlobalConst.PLAYER_FACE_TAG))
            {
                Debug.DrawRay(transform.position + transform.forward * 5, raycastDirection, Color.red, 0.05f);
                lastPlayerPosition = player.position;
                canSeePlayer = true;
            }
            else
            {
                canSeePlayer = false;
            }
        }  
    }

    void Turn()
    {
		Vector3 direction = player.position + new Vector3 (0, 2, 0) - transform.position;
        direction.Normalize();
        Vector3 directionDiff = direction - transform.forward;
		directionDiff *= rotationSpeed;
        transform.forward += directionDiff;
    }

    void follow()
    {
        Vector3 target = lastPlayerPosition;
        target += offsetVector;
        Vector3 movementDirection = target - transform.position;
        movementDirection.Normalize();
		movementDirection *= movementSpeed;
		rigidbody.MovePosition (transform.position + movementDirection);
    }

    void StartShootingCycle ()
    {
        StartCoroutine (shoot ());
    }

    IEnumerator shoot ()
    {
        if (active && canSeePlayer)
        {
            Transform spawnPoint;
            spawnPoint = shootSide ? bulletSpawnLeft : bulletSpawnRight;
            shootSide = ! shootSide;
			float shootChance = (float) rand.NextDouble ();

			if (shootChance <= ENEMY_SHOOT_CHANCE)
			{
				if (ShootBullet != null)
				{
					ShootBullet (Bullet.ShootType.NORMAL, Bullet.BulletType.ENEMY, spawnPoint, Vector3.zero);
				}
			}
        }
        
        yield return new WaitForSeconds (1f);

        StartShootingCycle();
    }

	public void Spawn (Vector3 position)
	{
		gameObject.SetActive (true);
		transform.position = position;
		spawnAnimation ();
	}

	public void spawnAnimation ()
	{
		Hashtable parameters = ITweenHelper.getFadeParameters (1f, 0.5f * spawnAnimationDuration);
		parameters.Add (ITweenHelper.OnCompleteTarget, this.gameObject);
		parameters.Add (ITweenHelper.OnComplete, "OnSpawnCilinderShowed");
		parameters.Add (ITweenHelper.Delay, 0.1f);
		iTween.FadeTo (spawnCilinder, parameters);

		parameters = ITweenHelper.getScaleParameters (spawnCilinderDefaultScale, 0.5f * spawnAnimationDuration);
		parameters.Add (ITweenHelper.Delay, 0.1f);
		iTween.ScaleTo (spawnCilinder, parameters);
	}

	public void OnSpawnCilinderShowed ()
	{
		Hashtable parameters = ITweenHelper.getFadeParameters (0f, 0.5f * spawnAnimationDuration);
		parameters.Add (ITweenHelper.OnCompleteTarget, this.gameObject);
		parameters.Add (ITweenHelper.OnComplete, "OnSpawnCompleted");
		iTween.FadeTo (spawnCilinder, parameters);

		parameters = ITweenHelper.getScaleParameters (Vector3.one, 0.25f * spawnAnimationDuration);
		iTween.ScaleTo (model.gameObject, parameters);
	}

	void OnSpawnCompleted ()
	{
		enemyHealth.SetEyesActive (true);
		active = true;
		StartShootingCycle ();
	}

	void Disable ()
	{
		active = false;
		enemyHealth.SetEyesActive (false);

		Hashtable parameters = ITweenHelper.getFadeParameters (0f, 0.01f);
		iTween.FadeTo (spawnCilinder, parameters);

		model.localScale = Vector3.zero;
		spawnCilinder.transform.localScale = spawnCilinderMinScale;

		gameObject.SetActive (false);
	}
}
