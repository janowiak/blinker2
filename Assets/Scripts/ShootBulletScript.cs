﻿using UnityEngine;
using System.Collections;

public class ShootBulletScript : MonoBehaviour {

	[SerializeField] float speed = 10;
	[SerializeField] int damage = 10;

    private CounteringScript counteringScript;
    private new Transform camera;
    private Rigidbody rb;

	// Use this for initialization
	void Start () 
	{
        rb = GetComponent<Rigidbody>();
        rb.AddForce(transform.forward * speed);

		counteringScript = GameObject.FindGameObjectWithTag (GlobalConst.PLAYER_TAG).GetComponent <CounteringScript>();
		camera = GameObject.FindGameObjectWithTag (GlobalConst.MAIN_CAMERA_TAG).GetComponent <Camera>().transform;
    }
	
    void OnCollisionEnter(Collision collision)
    {
        Destroy(this.gameObject);
    }
}
