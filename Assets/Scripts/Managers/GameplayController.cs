﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameplayController : MonoBehaviour {

    [SerializeField] GameObject player;
    [SerializeField] GameObject bulletPrefab;
    [SerializeField] GameObject bulletPool;

	[SerializeField] PowerUpSpawner powerUpSpawner;
	[SerializeField] PointsCounter pointsCounter;
	[SerializeField] EnemySpawner enemySpawner;

	[SerializeField] GameObject bulletHole;
	[SerializeField] int maxBulletHoles;

    Queue <Bullet> bullets;
	Queue <GameObject> bulletHoles;

    const int bulletPoolSize = 50;
	static GameplayController instance;
	static System.Random rand = new System.Random ();

	float gameTimer = 0;
	bool isGameStarted = false;

	public static GameplayController Instance 
	{
		get { return instance; }
	}

	void OnEnemyDeath ()
	{
		pointsCounter.AddPoints (GlobalConst.ENEMY_DEATH_POINTS);
	}

	void OnDamageToEnemyDealt ()
	{
		pointsCounter.AddPoints (GlobalConst.ENEMY_HIT_POINTS);
	}

	void Update ()
	{
		if (isGameStarted)
		{
			gameTimer += Time.deltaTime;
		}
	}

	void Start () 
	{
		instance = this;
		player.GetComponent <ShootingScript> ().ShootBullet += ShootBullet;
		enemySpawner.InitEnemies (ShootBullet, OnEnemyDeath, OnDamageToEnemyDealt);
        initBulletPool ();

		StartGame ();
	}

	public Transform GetClosestEnemy (Vector3 position)
	{
		return enemySpawner.GetClosestEnemy (position);
	}

	void StartGame ()
	{
		gameTimer = 0;
		isGameStarted = true;

		enemySpawner.StartSpawningEnemies ();
		powerUpSpawner.StartSpawnCycle ();
		pointsCounter.StartPointsTimer ();
		initBulletHoles ();
	}

	void initBulletHoles ()
	{
		bulletHoles = new Queue <GameObject> ();
	}

	void addBulletHole (ContactPoint hit, Transform parent)
	{
		if (bulletHoles.Count < maxBulletHoles)
		{
			GameObject newHole = Instantiate (bulletHole, hit.point + (hit.normal * 0.001f), Quaternion.LookRotation (hit.normal));
			newHole.transform.SetParent (parent);

			bulletHoles.Enqueue (newHole);
		}
		else
		{
			GameObject hole = bulletHoles.Dequeue ();

			hole.transform.SetParent (parent);
			hole.transform.position = hit.point + (hit.normal * 0.001f);
			hole.transform.rotation = Quaternion.LookRotation (hit.normal);

			bulletHoles.Enqueue (hole);
		}
	}

    void initBulletPool ()
    {
        bullets = new Queue<Bullet> ();

        for (int i = 0; i < bulletPoolSize; i++)
        {
            GameObject newBullet = Instantiate (bulletPrefab);
            newBullet.SetActive (false);
            newBullet.transform.SetParent (bulletPool.transform);
            Bullet bullet = newBullet.GetComponent <Bullet> ();

            bullet.OnCollision += OnBulletCollision;

            bullets.Enqueue (bullet);
        }
    }

	void ShootBullet (Bullet.ShootType shootType, Bullet.BulletType type, Transform spawnTransform, Vector3 speed)
    {
		Bullet bullet;

		switch (shootType)
		{
		case Bullet.ShootType.NORMAL:
			bullet = bullets.Dequeue ();
			bullet.Shoot (shootType, type, spawnTransform, speed);
			bullets.Enqueue (bullet);
			break;

		case Bullet.ShootType.SPREAD:
			
				for (int i = 0; i < Bullet.SPREAD_SHOT_BULLETS_COUNT; i ++)
			{
				bullet = bullets.Dequeue ();
				bullet.Shoot (shootType, type, spawnTransform, speed, true);
				bullets.Enqueue (bullet);
			}

			break;

		case Bullet.ShootType.AUTO_AIM:
			bullet = bullets.Dequeue ();
			bullet.Shoot (shootType, type, spawnTransform, speed);
			bullets.Enqueue (bullet);
			break;
		}
    }

	private void OnBulletCollision (GameObject sender, GameObject collider, Collision collision)
    {
		if (sender.tag == GlobalConst.PLAYER_BULLET_TAG || collider.tag != GlobalConst.PLAYER_BULLET_TAG)
        {
            destroyBullet (sender);

			if (collider.tag == GlobalConst.WALL_TAG)
			{
				ContactPoint hit = collision.contacts [0];
				//addBulletHole (hit, collider.transform);
			}
        }

		if (collider.tag == GlobalConst.ENEMY_BULLET_TAG || sender.tag != GlobalConst.ENEMY_BULLET_TAG)
        {
            destroyBullet (collider);
        }

		if ( (sender.tag == GlobalConst.PLAYER_BULLET_TAG &&  collider.tag == GlobalConst.NPC_TAG) ||
			(sender.tag == GlobalConst.ENEMY_BULLET_TAG &&  collider.tag == GlobalConst.PLAYER_TAG))
        {
            HealthScript health = collider.GetComponent<HealthScript> ();
            Bullet bullet = sender.GetComponent<Bullet> ();

            if (health != null && bullet != null)
            {
				health.DealDamage (bullet.Damage, sender.transform.position);
            }
        }
    }

    private void destroyBullet (GameObject bulletObject)
    {
        Bullet bullet = bulletObject.GetComponent<Bullet> ();

        if (bullet != null)
        {
            bullet.Disable ();
        }
    }
}
