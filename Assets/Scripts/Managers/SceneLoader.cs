﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour {

    [SerializeField] AnimateLoading animateLoading;
    [SerializeField] GameObject loadingCanvas;

    private static SceneLoader instance;

	const float LOADING_FADE_DURATION = 0.5f;
	const int MAIN_MENU_ID = 2;
	const int TUTORIAL_ID = 1;
	const int NEW_GAME_ID = 0;

    public static SceneLoader Instance
    {
        get
        {
            return instance;
        }
    }

	void Awake ()
	{
		instance = this;
	}

    private void Start()
    {
        instance = this;
        LoadMainMenu ();
    }

    public void LoadMainMenu (CanvasGroup prevCanvas = null)
    {
		StartCoroutine (LoadScene (MAIN_MENU_ID, prevCanvas));
    }

	public void LoadTutorial (CanvasGroup prevCanvas)
    {
		StartCoroutine (LoadScene (TUTORIAL_ID, prevCanvas));
    }

	public void LoadNewGame (CanvasGroup prevCavnas)
	{
		StartCoroutine (LoadScene (NEW_GAME_ID, prevCavnas));
	}

    IEnumerator LoadScene(int sceneNumber, CanvasGroup prevCanvas)
    {
        if (prevCanvas != null)
        {
            prevCanvas.gameObject.GetComponent<Fade> ().FadeOut ();
        }

        loadingCanvas.GetComponent <Fade> ().FadeIn ();
        animateLoading.StartAnimation ();

        AsyncOperation async = SceneManager.LoadSceneAsync (sceneNumber, LoadSceneMode.Additive);
        yield return async;

        loadingCanvas.GetComponent <Fade> ().FadeOut ();
        animateLoading.StopAnimation ();
    }

}
