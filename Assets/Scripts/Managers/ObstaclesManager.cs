﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstaclesManager : MonoBehaviour {

    [SerializeField] List <Collider> obstacles;

    public List<Tuple<Transform, float>> GetNClosestObstacles (Vector3 position, int n)
    {

        return AIHelper.GetNClosestToColliders (obstacles, n, position);
    }
}
