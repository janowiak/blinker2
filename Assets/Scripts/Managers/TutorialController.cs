﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialController : MonoBehaviour {

	[SerializeField] List <TriggerScript> triggers;
	[SerializeField] GameObject player;

	const string tutorialText1 = "{0} - MOVEMENT\n" +
	                             "{1} - JUMP";
	const string tutorialText2 = "DOUBLE TAP {0} TO DO A DOUBLE JUMP.\n";
	const string tutorialText3 = "CONTACT WITH WALLS ALLOWS\n" +
	                             "YOU TO RESET YOUR JUMP COUNTER\n" +
	                             "AND PERFORM ANOTHER\n" +
	                             "DOUBLE JUMP.";
	const string tutorialText4 = "{0} ALONG WITH {1} KEYS ALLOWS\n" +
	                             "YOU TO PERFORM A QUICK BLINK.\n" +
	                             "USE THAT TO REACH NEXT PLATFORM";
	const string tutorialText5 = "USE {0} BUTTON TO THROW YOUR SWORD.\n" +
	                             "PRESS {0} BUTTON SECOND TIME TO\n" +
	                             "TELEPORT TO YOUR SWORD.\n" +
	                             "USE THAT TO REACH THE UPPER PLATFORM.";
	const string tutorialText6 = "USE {0} TO SHOOT WITH YOUR PISTOL.\n" +
	                              "SHOOT ALL THREE TARGETS TO UNLOCK PATH TO\n" +
	                              "THE NEXT PLATFORM.";
	const string tutorialText7 = "WITH {0} BUTTON YOU CAN BLOCK AND COUNTER\n" +
	                             "ENEMY BULLETS\n\n" +
	                             "PERFORMING BLINK JUST BEFORE ENEMY BULLET HITS\n" +
	                             "YOU ACTIVATES THE BULLET TIME.\n\n" +
	                             "TRAIN A BIT BEFORE MOVING TO THE NEXT SECTION.";
	const string tutorialText8 = "BLINK FORWARD ({0} + {1}) TO ATTACK WITH YOUR SWORD.\n\n" +
	                             "ATTACKING AN ENEMY SHOOTS YOU UP, RESETS\n" +
	                             "YOUR JUMP COUNTER AND ACTIVATES A BULLET TIME.\n\n" +
	                             "TRY RREACHING NEXT PLATFORM USING THIS ABILITIES.\n\n" +
	                             "(FOR THE SAKE OF THIS TUTORIAL SECTION YOUR ABILITY\n" +
	                             "TO THROW A SWORD WILL BE DISABLED.)";
	const string tutorialText9 = "GOOD LUCK!";

	void Start ()
	{
		KeyManager keyManager = KeyManager.Instance;

		triggers [0].SetText (string.Format (tutorialText1, keyManager.GetKeyForGameAction (KeyManager.GameAction.FORWARD).ToString () +
			keyManager.GetKeyForGameAction (KeyManager.GameAction.BACKWARD).ToString () + 
			keyManager.GetKeyForGameAction (KeyManager.GameAction.LEFT).ToString () + 
			keyManager.GetKeyForGameAction (KeyManager.GameAction.RIGHT).ToString (),
			keyManager.GetKeyForGameAction (KeyManager.GameAction.JUMP).ToString ()));

		triggers [1].SetText (string.Format (tutorialText2, keyManager.GetKeyForGameAction (KeyManager.GameAction.JUMP).ToString ()));

		triggers [2].SetText (tutorialText3);

		triggers [3].SetText (string.Format (tutorialText4, keyManager.GetKeyForGameAction (KeyManager.GameAction.BLINK).ToString (),
			keyManager.GetKeyForGameAction (KeyManager.GameAction.FORWARD).ToString () +
			keyManager.GetKeyForGameAction (KeyManager.GameAction.BACKWARD).ToString () + 
			keyManager.GetKeyForGameAction (KeyManager.GameAction.LEFT).ToString () + 
			keyManager.GetKeyForGameAction (KeyManager.GameAction.RIGHT).ToString ()));

		triggers [4].SetText (string.Format (tutorialText5, keyManager.GetKeyForGameAction (KeyManager.GameAction.THROW).ToString ()));

		triggers [5].SetText (string.Format (tutorialText6, keyManager.GetKeyForGameAction (KeyManager.GameAction.SHOOT).ToString ()));

		triggers [6].SetText (string.Format (tutorialText7, keyManager.GetKeyForGameAction (KeyManager.GameAction.COUNTER).ToString ()));

		triggers [7].SetText (string.Format (tutorialText8, keyManager.GetKeyForGameAction (KeyManager.GameAction.BLINK).ToString (),
			keyManager.GetKeyForGameAction (KeyManager.GameAction.FORWARD).ToString ()));

		triggers [8].SetText (tutorialText9);
	}

	void OnEnable ()
	{
		for (int i = 0; i < triggers.Count; i++)
		{
			triggers [i].OnPlayerEnteredTrigger += OnPlayerEnteredTrigger;
			triggers [i].OnPlayerExitedTrigger += OnPlayerExitedTrigger;
		}
	}

	void OnDisable ()
	{
		for (int i = 0; i < triggers.Count; i++)
		{
			triggers [i].OnPlayerEnteredTrigger -= OnPlayerEnteredTrigger;
			triggers [i].OnPlayerExitedTrigger -= OnPlayerExitedTrigger;
		}
	}

	void OnPlayerEnteredTrigger (TriggerScript trigger)
	{
		trigger.ShowText ();

		switch (trigger.TriggerType)
		{
			case TriggerScript.Type.ENABLE_BLINK:
				player.GetComponent <BlinkingScript>().SetActive (true);
				break;

			case TriggerScript.Type.ENABLE_THROW:
				player.GetComponent <ThrowingScript>().SetActive (true);
				break;

			case TriggerScript.Type.ENABLE_SHOOT:
				player.GetComponent <ShootingScript>().SetActive (true);
				break;

			case TriggerScript.Type.DISABLE_THROW:
				player.GetComponent <ThrowingScript>().SetActive (false);
				break;

			case TriggerScript.Type.ENABLE_COUNTER:
				player.GetComponent <CounteringScript>().SetActive (true);
				break;

			default:
				break;
		}
	}

	void OnPlayerExitedTrigger (TriggerScript trigger)
	{
		trigger.HideText ();
	}
}
