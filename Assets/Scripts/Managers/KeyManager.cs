﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class KeyManager
{
	public enum GameAction
	{
		FORWARD, BACKWARD, RIGHT, LEFT, JUMP, SHOOT, BLINK, THROW, COUNTER, INTERACTION
	}

	KeyCode defaultForward = KeyCode.W;
	KeyCode defaultBackward = KeyCode.S;
	KeyCode defaultRight = KeyCode.D;
	KeyCode defaultLeft = KeyCode.A;
	KeyCode defaultJump = KeyCode.Space;
	KeyCode defaultShoot = KeyCode.Mouse0;
	KeyCode defaultBlink = KeyCode.Mouse1;
	KeyCode defaultThrow = KeyCode.LeftShift;
	KeyCode defaultCounter = KeyCode.E;
	KeyCode defaultInteraction = KeyCode.F;

	static KeyManager instance;

	Hashtable keyBindings;

	public static KeyManager Instance
	{
		get 
		{
			if (instance == null)
			{
				instance = new KeyManager ();
			}

			return instance;
		}
	}

	private KeyManager ()
	{
		loadKeyBindings ();
	}

	public List <GameAction> GetGameActionList ()
	{
		List <GameAction> result = new List <GameAction> ();

		foreach (GameAction gameAction in Enum.GetValues (typeof (GameAction)))
		{
			result.Add (gameAction);
		}

		return result;
	}

	public KeyCode GetKeyForGameAction (GameAction gameAction)
	{
		KeyCode result = KeyCode.None;

		if (keyBindings != null)
		{
			if (keyBindings.ContainsKey (gameAction))
			{
				result = (KeyCode) keyBindings [gameAction];
			}
			else
			{
				Debug.LogError ("Brak wartości klawisza dla danego GameAction");
			}
		}
		else
		{
			//TODO
			Debug.LogError ("Nie zainicjalizowano keyBindings");
		}

		return result;
	}

	public bool SetKeyForGameAction (KeyCode keyCode, GameAction gameAction)
	{
		bool result = false;

		if (keyBindings != null)
		{
			if (keyBindings.ContainsKey (gameAction))
			{
				keyBindings [gameAction] = keyCode;
			}
			else
			{
				keyBindings.Add (gameAction, keyCode);
			}

			result = true;
		}
		else
		{
			Debug.LogError ("Nie zainicjalizowano keyBindings");
		}

		return result;
	}

	public void SaveKeyBindings ()
	{
		PlayerPrefsManager.SaveKeyBindings (keyBindings);
	}

	void loadKeyBindings ()
	{
		keyBindings = PlayerPrefsManager.GetKeyBindings ();
		ekeKeyBindigs (keyBindings);
	}

	void loadDefaultKeyBindings ()
	{
		keyBindings = new Hashtable ();

		foreach (GameAction gameAction in Enum.GetValues (typeof (GameAction)))
		{
			keyBindings.Add (gameAction, getDefaultKeyForGameAction (gameAction));
		}
	}

	void ekeKeyBindigs (Hashtable keyBindigs)
	{
		foreach (KeyManager.GameAction gameAction in Enum.GetValues (typeof (KeyManager.GameAction)))
		{
			if (! keyBindigs.ContainsKey (gameAction))
			{
				keyBindigs.Add (gameAction, getDefaultKeyForGameAction (gameAction));
			}
		}
	}

	KeyCode getDefaultKeyForGameAction (GameAction gameAction)
	{
		KeyCode result = KeyCode.None;

		switch (gameAction)
		{
			case GameAction.FORWARD:
				result = defaultForward;

				break;

			case GameAction.BACKWARD:
				result = defaultBackward;

				break;

			case GameAction.RIGHT:
				result = defaultRight;

				break;

			case GameAction.LEFT:
				result = defaultLeft;

				break;

			case GameAction.JUMP:
				result = defaultJump;

				break;

			case GameAction.SHOOT:
				result = defaultShoot;

				break;

			case GameAction.BLINK:
				result = defaultBlink;

				break;

			case GameAction.THROW:
				result = defaultThrow;

				break;

			case GameAction.COUNTER:
				result = defaultCounter;

				break;

			case GameAction.INTERACTION:
				result = defaultInteraction;

				break;
		}

		return result;
	}

	public string GetGameActionName (GameAction gameAction)
	{
		string result = "NONE";

		switch (gameAction)
		{
			case GameAction.FORWARD:
				result = "FORWARD";

				break;

			case GameAction.BACKWARD:
				result = "BACKWARD";

				break;

			case GameAction.RIGHT:
				result = "RIGHT";

				break;

			case GameAction.LEFT:
				result = "LEFT";

				break;

			case GameAction.JUMP:
				result = "JUMP";

				break;

			case GameAction.SHOOT:
				result = "SHOOT";

				break;

			case GameAction.BLINK:
				result = "BLINK";

				break;

			case GameAction.THROW:
				result = "THROW";

				break;

			case GameAction.COUNTER:
				result = "COUNTER";

				break;

			case GameAction.INTERACTION:
				result = "INTERACTION";

				break;
		}

		return result;
	}
}
