﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointsCounter : MonoBehaviour {

	public delegate void OnPointsValueChangedEventHandler ();
	public event OnPointsValueChangedEventHandler OnPointsValueChanged;

	[SerializeField] Text pointsLabel; 

	int points = 0;

	public int Points
	{
		get { return points; }
	}

	public void ResetPoints ()
	{
		points = 0;
	}

	public void AddPoints (int pointsToAdd)
	{
		points += pointsToAdd;
		OnPointsChanged ();
	}

	public void StartPointsTimer ()
	{
		StopPointsTimer (); 
		StartCoroutine (pointsTimer ());
	}

	public void StopPointsTimer ()
	{
		StopCoroutine (pointsTimer ());
	}

	IEnumerator pointsTimer ()
	{
		while (true)
		{
			yield return new WaitForSeconds (1f);
			points ++;
			OnPointsChanged ();
		}
	}

	void OnPointsChanged ()
	{
		pointsLabel.text = "Points: " + points;

		if (OnPointsValueChanged != null)
		{
			OnPointsValueChanged ();
		}
	}
}
