﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PowerUpSpawner : MonoBehaviour {

	[SerializeField] GameObject spreadShotPowerUpPrefab;
	[SerializeField] GameObject autoAimPowerUpPrefab;
	[SerializeField] GameObject staminaPowerUp;
	[SerializeField] GameObject movemenPowerUp;
	[SerializeField] GameObject fireRatePowerUp;
	[SerializeField] GameObject smallHealthPackPowerUp;
	[SerializeField] GameObject largeHealthPackPowerUp;
	[SerializeField] Transform powerUpsPool;

	System.Random random = new System.Random ();

	public const float MIN_DELAY_TIME = 5f;
	public const float MAX_DELAY_TIME = 15f;
	public const float MAX_POSITION_X = 200f;
	public const float MAX_POSITION_Y = 50f;
	public const float MAX_POSITION_Z = 200f;
	public const float MIN_POSITION_Y = 25f;

	public void StartSpawnCycle ()
	{
		StopSpawnCycle ();
		StartCoroutine (spawnCycle ());
	}

	public void StopSpawnCycle ()
	{
		StopCoroutine (spawnCycle ());
	}

	IEnumerator spawnCycle ()
	{
		while (true)
		{
			float delay = (float) (random.NextDouble () * (MAX_DELAY_TIME - MIN_DELAY_TIME) + 5);

			yield return new WaitForSeconds (delay);

			int powerUpType = random.Next (Enum.GetNames (typeof (PowerUp.PowerUpType)).Length);
			spawnPowerUp ((PowerUp.PowerUpType) (Enum.ToObject (typeof (PowerUp.PowerUpType), powerUpType)));
		}
	}

	void spawnPowerUp (PowerUp.PowerUpType powerUpType)
	{
		GameObject powerUp = null;

		switch (powerUpType)
		{
			case PowerUp.PowerUpType.AUTO_AIM:
				powerUp = Instantiate (autoAimPowerUpPrefab);

				break;

			case PowerUp.PowerUpType.MOVEMENT:
				powerUp = Instantiate (movemenPowerUp);

				break;

			case PowerUp.PowerUpType.LARGE_HEALTH:
				powerUp = Instantiate (largeHealthPackPowerUp);

				break;

			case PowerUp.PowerUpType.SMALL_HEALTH:
				powerUp = Instantiate (smallHealthPackPowerUp);

				break;

			case PowerUp.PowerUpType.FIRE_RATE:
				powerUp = Instantiate (fireRatePowerUp);

				break;

			case PowerUp.PowerUpType.SPREAD_SHOT:
				powerUp = Instantiate (spreadShotPowerUpPrefab);

				break;

			case PowerUp.PowerUpType.STAMINA:
				powerUp = Instantiate (staminaPowerUp);

				break;
		}

		powerUp.SetActive (true);
		powerUp.transform.SetParent (powerUpsPool);
		powerUp.transform.position = getNewRandPosition ();
		powerUp.GetComponent <PowerUp> ().Spawn ();
	}

	Vector3 getNewRandPosition ()
	{
		float x = (float) (random.NextDouble () * MAX_POSITION_X);
		float y = (float) (random.NextDouble () * (MAX_POSITION_Y - MIN_POSITION_Y)) + MIN_POSITION_Y;
		float z = (float) (random.NextDouble () * MAX_POSITION_Z);

		Vector3 newPosition = powerUpsPool.position + new Vector3 (x, y, z);

		return newPosition;
	}

}
