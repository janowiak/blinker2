﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RenderTextureSetter : MonoBehaviour {

	[SerializeField] List <Camera> cameras;
	[SerializeField] Material renderTextureMaterial;

	static RenderTexture renderTexture;
	const int screenWidth = 240;

	void Awake ()
	{
		float aspectRatio = (float) Screen.height / Screen.width;
		Debug.Log ("Resolution: " + screenWidth + " x " + (int) (screenWidth * aspectRatio));
		renderTexture = new RenderTexture (screenWidth, (int) (screenWidth * aspectRatio), 24);
		renderTextureMaterial.mainTexture = renderTexture;
		renderTexture.antiAliasing = 1;
		renderTexture.filterMode = FilterMode.Point;
			
		for (int i = 0; i < cameras.Count; i ++)
		{
			if (cameras [i].targetTexture != null)
			{
				cameras [i].targetTexture.Release ();
			}

			cameras [i].targetTexture = renderTexture;
		}
	}
}
