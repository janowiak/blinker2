﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ITweenHelper : MonoBehaviour {

	public const string Alpha = "alpha";
	public const string From = "from";
	public const string To = "to";
	public const string Time = "time";
	public const string Scale = "scale";
	public const string OnUpdateTarget = "onupdatetarget";
	public const string OnUpdate = "onupdate";
	public const string EaseType = "easetype";
	public const string OnComplete = "oncomplete";
	public const string OnCompleteTarget = "oncompletetarget";
	public const string Delay = "delay";
	public const string Name = "name";
	public const string Path = "path";

	public const string TextMeshKey = "TextMeshKey";
	public const string ValueKey = "ValueKey";

	public static Hashtable getFadeParameters (float alpha, float time, iTween.EaseType easeType = iTween.EaseType.linear)
	{
		Hashtable parameters = new Hashtable ();
		parameters.Add (Alpha, alpha);
		parameters.Add (Time, time);
		parameters.Add (EaseType, easeType);

		return parameters;
	}

	public static Hashtable getScaleParameters (Vector3 scale, float time, iTween.EaseType easeType = iTween.EaseType.linear)
	{
		Hashtable parameters = new Hashtable ();
		parameters.Add (Scale, scale);
		parameters.Add (Time, time);
		parameters.Add (EaseType, easeType);

		return parameters;
	}

	public static IEnumerator ChangeCanvasGroupAlpha (CanvasGroup canvasGroup, float alpha, float duration, iTween.EaseType easeType = iTween.EaseType.linear, float delay = 0)
	{
		if (delay > 0)
		{
			yield return new WaitForSeconds (delay);
		}

		RemoveScript <ITweenAttachment> (canvasGroup.gameObject);
		yield return new WaitWhile (() => canvasGroup.gameObject.GetComponent <ITweenAttachment> () != null);

		ChangeCanvasGroupAlpha script = AttachScript <ChangeCanvasGroupAlpha> (canvasGroup.gameObject);
		string scriptName = script.GetName ();
		Hashtable fadeToParameters = new Hashtable ();
		fadeToParameters.Add (To, alpha);
		fadeToParameters.Add (From, canvasGroup.alpha);
		fadeToParameters.Add (Time, duration);
		fadeToParameters.Add (EaseType, easeType);
		fadeToParameters.Add (OnUpdate, script.GetOnUpdateName ());

		iTween.ValueTo (script.gameObject, fadeToParameters);

		yield return new WaitForSeconds (duration);

		RemoveScript <ITweenAttachment> (canvasGroup.gameObject);

		yield return new WaitWhile (() => canvasGroup.gameObject.GetComponent <ITweenAttachment> () != null);
	}

	public static IEnumerator ChangeUIGroupAlpha (GameObject parent, float alpha, float duration, iTween.EaseType easeType = iTween.EaseType.linear, float delay = 0)
	{
		if (delay > 0)
		{
			yield return new WaitForSeconds (delay);
		}

		Image [] images = parent.GetComponentsInChildren <Image> ();
		Text [] texts = parent.GetComponentsInChildren <Text> ();

		//Detach tweener
		if (images != null)
		{
			for (int i = 0; i < images.Length; i++)
			{
				RemoveScript <ITweenAttachment> (images [i].gameObject);
				yield return new WaitWhile (() => images [i].gameObject.GetComponent <ITweenAttachment> () != null);
			}
		}

		if (texts != null)
		{
			for (int i = 0; i < texts.Length; i++)
			{
				RemoveScript <ITweenAttachment> (texts [i].gameObject);
				yield return new WaitWhile (() => texts [i].gameObject.GetComponent <ITweenAttachment>  () != null);
			}
		}

		//Attach new tweener
		if (images != null)
		{
			for (int i = 0; i < images.Length; i++)
			{
				ChangeUIElementColor script = AttachScript <ChangeUIElementColor> (images [i].gameObject);
				string scriptName = script.GetName ();
				Color colorTo = images [i].color;
				colorTo.a = alpha;
				Hashtable colorToParameters = getColorParameters (images [i].color, colorTo, duration, scriptName);
				colorToParameters.Add (OnUpdate, script.GetOnUpdateName ());
				colorToParameters.Add (OnUpdateTarget, script.gameObject);
				iTween.ValueTo (script.gameObject, colorToParameters);
			}
		}

		if (texts != null)
		{
			for (int i = 0; i < texts.Length; i++)
			{
				ChangeUIElementColor script = AttachScript <ChangeUIElementColor> (texts [i].gameObject);
				string scriptName = script.GetName ();
				Color colorTo = texts [i].color;
				colorTo.a = alpha;
				Hashtable colorToParameters = getColorParameters (texts [i].color, colorTo, duration, scriptName);
				colorToParameters.Add (OnUpdate, script.GetOnUpdateName ());
				colorToParameters.Add (OnUpdateTarget, script.gameObject);
				iTween.ValueTo (script.gameObject, colorToParameters);
			}
		}

		yield return new WaitForSeconds (duration);

		//Detach tweener
		if (images != null)
		{
			for (int i = 0; i < images.Length; i++)
			{
				RemoveScript <ITweenAttachment> (images [i].gameObject);
				yield return new WaitWhile (() => images [i].gameObject.GetComponent <ITweenAttachment> () != null);
			}
		}

		if (texts != null)
		{
			for (int i = 0; i < texts.Length; i++)
			{
				RemoveScript <ITweenAttachment> (texts [i].gameObject);
				yield return new WaitWhile (() => texts [i].gameObject.GetComponent <ITweenAttachment>  () != null);
			}
		}
	}

	public static void RemoveScript <T> (GameObject gameObject) where T : ITweenAttachment
	{
		T attachedScript = gameObject.GetComponent <T> ();

		if (attachedScript != null)
		{
			string name = attachedScript.GetName ();
			iTween.StopByName (gameObject, name);
			Destroy (attachedScript);
		}
	}

	public static T AttachScript <T> (GameObject gameObject) where T : ITweenAttachment
	{
		T attachedScript = gameObject.AddComponent <T> ();

		return attachedScript;
	}

	public static Hashtable getColorParameters (Color colorFrom, Color colorTo, float duration, string name, iTween.EaseType easeType = iTween.EaseType.linear, float delay = 0)
	{
		Hashtable parameters = new Hashtable ();
		parameters.Add (Time, duration);
		parameters.Add (EaseType, easeType);
		parameters.Add (From, colorFrom);
		parameters.Add (To, colorTo);
		parameters.Add (Delay, delay);
		parameters.Add (Name, name);

		return parameters;
	}
}
