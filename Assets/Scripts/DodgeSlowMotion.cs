﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DodgeSlowMotion : MonoBehaviour
{
	[SerializeField] SlowMotion slowMotion;

    void OnTriggerEnter(Collider other)
    {
		if (other.tag == GlobalConst.ENEMY_BULLET_TAG && slowMotion.CanSlowMotion ())
		{
			slowMotion.startSlowMotion();
		}
    }
}
