﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsMenu : MainMenuWindow
{
    [SerializeField] Slider soundSlider;
    [SerializeField] Slider mouseSenitivity;
    [SerializeField] Button controlsButton;

    private void Start ()
    {
        if (controlsButton != null)
        {
            controlsButton.onClick.AddListener (() => OnControlsButtonClicked ());
        }
    }

    public override void Show()
    {
        loadPrefs ();
        base.Show ();
    }

    void OnControlsButtonClicked ()
    {
        savePrefs ();

        MainMenuManager.Intance.SwitchToControlsMenu (this);
    }

    protected override void OnBackButtonClicked()
    {
        base.OnBackButtonClicked ();
        savePrefs ();

        MainMenuManager.Intance.SwitchToMainMenu (this);
    }

    void savePrefs ()
    {
        PlayerPrefsManager.SaveOptions (soundSlider.value, mouseSenitivity.value);
    }

    void loadPrefs ()
    {
        soundSlider.value = PlayerPrefsManager.GetSoundValue ();
        mouseSenitivity.value = PlayerPrefsManager.GetMouseSesitivity ();
    }


}
