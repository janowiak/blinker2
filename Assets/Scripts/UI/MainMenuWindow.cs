﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuWindow : MonoBehaviour {

	[SerializeField] protected CanvasGroup canvasGroup;
	[SerializeField] protected Button backButton;

	bool visible = false;

	protected virtual void Awake ()
	{
		if (backButton != null)
		{
			backButton.onClick.AddListener (() => OnBackButtonClicked ());
		}
	}

	protected virtual void OnBackButtonClicked ()
	{
		
	}

	public bool IsVisible
	{
		get { return visible; }
	}

	public virtual void Show ()
	{
		iTween.Stop (canvasGroup.gameObject);
		StartCoroutine (showWindowAnimation ());
	}

	public virtual void Close ()
	{
		iTween.Stop (canvasGroup.gameObject);
		StartCoroutine (hideWindowAnimation ());
	}

	IEnumerator showWindowAnimation ()
	{
		yield return StartCoroutine (ITweenHelper.ChangeCanvasGroupAlpha (canvasGroup, 1f, AnimationConsts.QUICK_FADE_ANIMATION_DURATION));

		canvasGroup.blocksRaycasts = true;
		canvasGroup.interactable = true;

		visible = true;
	}

	IEnumerator hideWindowAnimation ()
	{
		yield return StartCoroutine (ITweenHelper.ChangeCanvasGroupAlpha (canvasGroup, 0f, AnimationConsts.QUICK_FADE_ANIMATION_DURATION));

		canvasGroup.blocksRaycasts = false;
		canvasGroup.interactable = false;

		visible = false;
	}
}
