﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InteractionText : MonoBehaviour {

	[SerializeField] Text textLabel;

	bool isVisible = false;

	public bool IsVisible
	{
		get { return isVisible; }
	}

	public void Show (string text)
	{
		isVisible = true;
		textLabel.text = text;
		iTween.Stop (this.gameObject);
		iTween.ValueTo (this.gameObject, getFadeParameters (1f));
	}

	public void Hide ()
	{
		isVisible = false;
		iTween.Stop (this.gameObject);
		iTween.ValueTo (this.gameObject, getFadeParameters (0));
	}

	void OnFadeUpdate (float alpha)
	{
		ImageProcessing.ChangeTextAlpha (textLabel, alpha);
	}

	Hashtable getFadeParameters (float alpha)
	{
		Hashtable parameters = new Hashtable ();
		parameters.Add (ITweenHelper.From, textLabel.color.a);
		parameters.Add (ITweenHelper.To, alpha);
		parameters.Add (ITweenHelper.Time, AnimationConsts.QUICK_FADE_ANIMATION_DURATION);
		parameters.Add (ITweenHelper.EaseType, iTween.EaseType.easeInOutQuad);
		parameters.Add (ITweenHelper.OnUpdate, "OnFadeUpdate");
		parameters.Add (ITweenHelper.OnUpdateTarget, this.gameObject);

		return parameters;
	}
}
