﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerUpUI : MonoBehaviour {

	[SerializeField] Image slider;
	[SerializeField] Text text;

	public void FillSlider (float fillAmount)
	{
		slider.fillAmount = fillAmount;
	}

	public void SetColor (Color color)
	{
		slider.color = color;
	}

	public void SetText (string text)
	{
		this.text.text = text;
	}
}
