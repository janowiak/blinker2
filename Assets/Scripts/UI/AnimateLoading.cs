﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimateLoading : MonoBehaviour {

	const float animationDelay = 0.1f;

    public void StartAnimation ()
    {
        StopAllCoroutines ();
        StartCoroutine (Animate ());
    }

    public void StopAnimation ()
    {
        StopAllCoroutines ();
    }

    IEnumerator Animate ()
    {
        while (true)
        {
            Text loadingText = GetComponent<Text> ();
            loadingText.text = "Loading";
            yield return new WaitForSeconds (animationDelay);
            loadingText.text = "Loading.";
            yield return new WaitForSeconds (animationDelay);
            loadingText.text = "Loading..";
            yield return new WaitForSeconds (animationDelay);
            loadingText.text = "Loading...";
            yield return new WaitForSeconds (0.3f);
        }
    }
}
