﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuManager : MonoBehaviour {

	[SerializeField] MainMenuWindow mainMenu;
	[SerializeField] MainMenuWindow controlsWindow;
    [SerializeField] OptionsMenu optionsMenu;

	List <MainMenuWindow> allWindows = new List <MainMenuWindow> ();

	static MainMenuManager instance;

	public static MainMenuManager Intance
	{
		get { return instance; }
	}

	void Awake ()
	{
		instance = this;
		fillAllWindowsList ();
	}

	void fillAllWindowsList ()
	{
		allWindows.Add (mainMenu);
		allWindows.Add (controlsWindow);
	}

	void closeAllExcept (MainMenuWindow exception)
	{
		for (int i = 0; i < allWindows.Count; i ++)
		{
			if (exception != allWindows [i])
			{
				allWindows [i].Close ();
			}
		}
	}

	public void SwitchToWindow (MainMenuWindow windowToSwitch)
	{
		closeAllExcept (windowToSwitch);
		windowToSwitch.Show ();
	}

	public void SwitchToMainMenu (MainMenuWindow prevWindow)
	{
		prevWindow.Close ();
		mainMenu.Show ();
	}

    public void SwitchToOptions (MainMenuWindow prevWindow)
    {
        prevWindow.Close ();
        optionsMenu.Show ();
    }

	public void SwitchToControlsMenu (MainMenuWindow prevWindow)
	{
		prevWindow.Close ();
		controlsWindow.Show ();
	}
}
