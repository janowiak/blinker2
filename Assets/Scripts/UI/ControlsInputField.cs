﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ControlsInputField : MonoBehaviour {

	[SerializeField] Text labelText;
	[SerializeField] Text keyText;
	[SerializeField] Image background;
	[SerializeField] GameObject inputField;
	[SerializeField] Button button;

	public delegate void OnControlsInputClickedEventHandler (ControlsInputField controlsInputField);
	public event OnControlsInputClickedEventHandler OnControlsInputClicked;
	public event OnControlsInputClickedEventHandler OnKeySelected;

	bool selected = false;
	const string pressKey = "Press Key";
	KeyCode currentKey;
	KeyManager.GameAction gameAction;

	public KeyCode KeyCode
	{
		get {return currentKey; }
		set { setKeyCode (value); }
	}

	public KeyManager.GameAction GameAction
	{
		get { return gameAction; }
	}

	void Start ()
	{
		if (button != null)
		{
			button.onClick.AddListener (() => OnButtonClicked ());
		}
	}

	void OnButtonClicked ()
	{
		if (OnControlsInputClicked != null)
		{
			OnControlsInputClicked (this);
		}
	}

	public void Setup (KeyManager.GameAction gameAction)
	{
		this.gameAction = gameAction;
		labelText.text = KeyManager.Instance.GetGameActionName (gameAction);
		setKeyCode (KeyManager.Instance.GetKeyForGameAction (gameAction));
	}

	public bool Selected
	{
		get { return selected; }
		set { setSelected (value); }
	}

	void setSelected (bool selected)
	{
		if (! this.selected && selected)
		{
			startSelectedAnimation ();
		}
		else if (this.selected && ! selected)
		{
			stopSelectedAnimation ();
		}

		this.selected = selected;
	}

	void startSelectedAnimation ()
	{
		stopSelectedAnimation ();
		StartCoroutine (animationCycle ());
	}

	void stopSelectedAnimation ()
	{
		StopCoroutine (animationCycle ());
		iTween.Stop (inputField.gameObject);

		ImageProcessing.ChangeUIGroupAlpha (inputField, 1f);
	}

	IEnumerator animationCycle ()
	{
		yield return StartCoroutine (ITweenHelper.ChangeUIGroupAlpha (inputField, 0, 0.5f));
		yield return StartCoroutine (ITweenHelper.ChangeUIGroupAlpha (inputField, 1, 0.5f));

		OnCycleAnimationCompleted ();
	}

	void OnCycleAnimationCompleted ()
	{
		if (selected)
		{
			StartCoroutine (animationCycle ());
		}
	}

	void Update ()
	{
		if (selected)
		{
			foreach (KeyCode kcode in Enum.GetValues (typeof (KeyCode)))
			{
				if (Input.GetKey (kcode))
				{
					if (kcode != KeyCode.Escape)
					{
						setKeyCode (kcode);

						if (OnKeySelected != null)
						{
							OnKeySelected (this);
						}
					}
				}
			}
		}
	}

	void setKeyCode (KeyCode newKeyCode)
	{
		this.currentKey = newKeyCode;

		if (currentKey == KeyCode.None)
		{
			keyText.text = "";
		}
		else
		{
			keyText.text = currentKey.ToString ();
		}
	}
}
