﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlsMenu : MainMenuWindow {

	[SerializeField] GameObject controlOptionPrefab;
	[SerializeField] Transform controlsContaner;

	List <ControlsInputField> controlOptions = new List <ControlsInputField> ();
	ControlsInputField currentSelected = null;

	protected override void Awake ()
	{
		base.Awake ();
		setupControls ();
	}

	protected override void OnBackButtonClicked ()
	{
		base.OnBackButtonClicked ();
		MainMenuManager.Intance.SwitchToOptions (this);
	}

	void setupControls ()
	{
		List <KeyManager.GameAction> gameActions = KeyManager.Instance.GetGameActionList ();

		for (int i = 0; i < gameActions.Count; i ++)
		{
			ControlsInputField controlsInputField = createControlOption (gameActions [i]);
			controlOptions.Add (controlsInputField);
		}
	}

	ControlsInputField createControlOption (KeyManager.GameAction gameAction)
	{
		GameObject newGameObject = Instantiate (controlOptionPrefab);
		newGameObject.SetActive (true);
		newGameObject.transform.SetParent (controlsContaner, false);

		ControlsInputField controlsInputField = newGameObject.GetComponent <ControlsInputField> ();
		controlsInputField.Setup (gameAction);
		controlsInputField.OnControlsInputClicked += OnControlsInputClicked;
		controlsInputField.OnKeySelected += OnKeySelected;

		return controlsInputField;
	}

	void OnControlsInputClicked (ControlsInputField controlsInputField)
	{
		if (currentSelected == null)
		{
			currentSelected = controlsInputField;
			currentSelected.Selected = true;
		}
		else
		{
			currentSelected.Selected = false;
			currentSelected = controlsInputField;
			currentSelected.Selected = true;
		}
	}

	void OnKeySelected (ControlsInputField controlsInputField)
	{
		for (int i = 0; i < controlOptions.Count; i ++)
		{
			if (controlOptions [i] != controlsInputField)
			{
				if (controlOptions [i].KeyCode == controlsInputField.KeyCode)
				{
					controlOptions [i].KeyCode = KeyCode.None;
				}
			}
		}
			
		if (controlsInputField == currentSelected)
		{
			currentSelected.Selected = false;
			currentSelected = null;
		}

		saveKeyBindings ();
	}

	void saveKeyBindings ()
	{
		for (int i = 0; i < controlOptions.Count; i ++)
		{
			KeyManager.Instance.SetKeyForGameAction (controlOptions [i].KeyCode, controlOptions [i].GameAction);
		}

		KeyManager.Instance.SaveKeyBindings ();
	}

	void Update ()
	{
		if (Input.GetKey (KeyCode.Escape))
		{
			if (currentSelected != null)
			{
				currentSelected.Selected = false;
				currentSelected = null;
			}
		}
	}
}
