﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fade : MonoBehaviour {

	const float FADE_DURATION = 0.4f;

	public void FadeIn ()
    {
        iTween.Stop (gameObject);

        CanvasGroup canvas = GetComponent<CanvasGroup> ();

        Hashtable iTweenHashtable = iTween.Hash (
			ITweenHelper.From, canvas.alpha,
			ITweenHelper.To, 1f,
			ITweenHelper.Time, FADE_DURATION,
			ITweenHelper.OnUpdateTarget, gameObject,
			ITweenHelper.OnUpdate, "setAlpha");

        iTween.ValueTo (gameObject, iTweenHashtable);

        canvas.interactable = true;
        canvas.blocksRaycasts = true;
    }

    public void FadeOut ()
    {
        iTween.Stop (gameObject);

        CanvasGroup canvas = GetComponent<CanvasGroup> ();

        Hashtable iTweenHashtable = iTween.Hash (
			ITweenHelper.From, GetComponent <CanvasGroup> ().alpha,
			ITweenHelper.To, 0f,
			ITweenHelper.Time, FADE_DURATION,
			ITweenHelper.OnUpdateTarget, gameObject,
			ITweenHelper.OnUpdate, "setAlpha",
			ITweenHelper.OnCompleteTarget, gameObject,
			ITweenHelper.OnComplete, "stopAnimate");

        iTween.ValueTo (gameObject, iTweenHashtable);

        canvas.interactable = false;
        canvas.blocksRaycasts = false;
    }

    void setAlpha (float alpha)
    {
        GetComponent <CanvasGroup> ().alpha = alpha;
    }

    void stopAnimate()
    {
        
    }
}
