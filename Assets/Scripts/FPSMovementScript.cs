﻿using UnityEngine;
using System.Collections;

public class FPSMovementScript : MonoBehaviour {

	float moveSpeed = GlobalConst.DEFAULT_MOVEMENT_SPEED;
	float jumpPower = GlobalConst.DEFAULT_JUMP_POWER;

    RigAnimationManager animManager;
    Rigidbody rb;
    ShootingScript shootingScript;
    SlowMotion slowMotion;
    ThrowingScript throwingScript;

    bool grounded = true;
    bool hasJumped = false;
    int maxJumps = 2;
    int jumpCounter = 0;
    bool canJump = true;
    float horMove;
    float verMove;

    // Use this for initialization
    void Start() 
	{
        animManager = GetComponent <RigAnimationManager>();
        shootingScript = GetComponent <ShootingScript>();
        slowMotion = GetComponent <SlowMotion>();
        throwingScript = GetComponent <ThrowingScript>();
        rb = GetComponent <Rigidbody>();
        rb.freezeRotation = true;
    }

	public void SetMovementSpeed (float movementSpeed)
	{
		this.moveSpeed = movementSpeed;
	}

	public void SetJumpPowerUp (float jumpPower)
	{
		this.jumpPower = jumpPower;
	}

    void Update()
    {
		if (Input.GetKeyDown (KeyManager.Instance.GetKeyForGameAction (KeyManager.GameAction.JUMP)) && canJump)
		{
			hasJumped = true;
		}
            
		if (jumpCounter >= maxJumps)
		{
			canJump = false;
		} 
		else
		{
			canJump = true;
		}
    }

    void FixedUpdate()
	{
		if (Input.GetKey (KeyManager.Instance.GetKeyForGameAction (KeyManager.GameAction.RIGHT)))
		{
			horMove = 1f;
		}
		else if (Input.GetKey (KeyManager.Instance.GetKeyForGameAction (KeyManager.GameAction.LEFT)))
		{
			horMove = -1f;
		}

		if (Input.GetKey (KeyManager.Instance.GetKeyForGameAction (KeyManager.GameAction.FORWARD)))
		{
			verMove = 1f;
		}
		else if (Input.GetKey (KeyManager.Instance.GetKeyForGameAction (KeyManager.GameAction.BACKWARD)))
		{
			verMove = -1f;
		}

		horMove = horMove * moveSpeed * Time.deltaTime;
		verMove = verMove * moveSpeed * Time.deltaTime;

		move (horMove, verMove);
            
        if (hasJumped)
        {
			jump ();
        }
    }

	void move (float horMove, float verMove)
	{
		transform.Translate(horMove, 0, verMove);
		float speed = new Vector2 (horMove, verMove).magnitude;

		if (grounded)
		{
			animManager.Move (speed);
		}
		else
		{
			animManager.Move(0);
		}
	}

	void jump ()
	{
		hasJumped = false;
		grounded = false;
		jumpCounter ++;
		rb.velocity = Vector3.zero;
		rb.AddForce (transform.up * jumpPower * (slowMotion.IsSlowMotion() ? 2 : 1));
	}

    void OnCollisionEnter (Collision other)
    {
		if ((other.gameObject.tag == GlobalConst.ENVIRONMENT_TAG || other.gameObject.tag == GlobalConst.WALL_TAG ||
		    other.gameObject.tag == GlobalConst.NPC_TAG) && !grounded)
		{
			Ground ();
		}
    }

    public void Ground()
    {
        grounded = true;
        jumpCounter = 0;
    }
}
