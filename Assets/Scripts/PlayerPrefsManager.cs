﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerPrefsManager
{

    const string SOUND_VALUE = "SoundValue";
    const string MOUSE_SENSITIVITY = "MouseSensitivity";

    public static void SaveKeyBindings (Hashtable keyBindings)
	{
		foreach (DictionaryEntry pair in keyBindings)
		{
			PlayerPrefs.SetString (pair.Key.ToString (), pair.Value.ToString ());
		}

		PlayerPrefs.Save ();
	}

    public static void SaveOptions (float soundValue, float mouseSensitivity)
    {
        PlayerPrefs.SetFloat (SOUND_VALUE, soundValue);
        PlayerPrefs.SetFloat (MOUSE_SENSITIVITY, mouseSensitivity);

        PlayerPrefs.Save ();
    }

    public static float GetSoundValue ()
    {
        float result = 0;
        result = PlayerPrefs.GetFloat (SOUND_VALUE);

        return result;
    }

    public static float GetMouseSesitivity()
    {
        float result = 0;
        result = PlayerPrefs.GetFloat (MOUSE_SENSITIVITY);

        return result;
    }

    public static Hashtable GetKeyBindings ()
	{
		Hashtable keyBindigs = new Hashtable ();
		KeyCode keyCodeValue;

		foreach (KeyManager.GameAction gameAction in Enum.GetValues (typeof (KeyManager.GameAction)))
		{
			if (PlayerPrefs.HasKey (gameAction.ToString ()))
			{
				keyCodeValue = (KeyCode) Enum.Parse (typeof (KeyCode), PlayerPrefs.GetString (gameAction.ToString ()));
				keyBindigs.Add (gameAction, keyCodeValue);
			}
		}

		return keyBindigs;
	}
}
