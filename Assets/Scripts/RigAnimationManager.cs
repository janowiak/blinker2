﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RigAnimationManager : MonoBehaviour {

	[SerializeField] Animator anim;

    private float idleTimer;
    private float idleTimerThreshold = 10;

    public enum State { NORMAL, RUN, ATTACK, THROW, SHOOT, COUNTER, WALLON, IDLE, ERR }

    // Update is called once per frame
    void Update()
    {
        ManageIdleAnimation();
    }

    void ManageIdleAnimation()
    {
        idleTimer += Time.deltaTime;

        if (idleTimer >= idleTimerThreshold)
        {
            anim.SetTrigger("idle");
            idleTimer = 0;
        }

		if (GetState () == State.IDLE && Input.anyKeyDown)
		{
			anim.SetTrigger("normal");
		}       
    }

    //Returns current animation state
    public State GetState()
    {
		State result = State.ERR;
        int currentStateHash = anim.GetCurrentAnimatorStateInfo(0).nameHash;

		if (currentStateHash == Animator.StringToHash ("Base.normal"))
		{
			result = State.NORMAL;
		}
        else if (currentStateHash == Animator.StringToHash ("Base.run"))
		{
			result = State.RUN;
		}
        else if (currentStateHash == Animator.StringToHash ("Base.attack1") ||
		          currentStateHash == Animator.StringToHash ("Base.attack2") ||
		          currentStateHash == Animator.StringToHash ("Base.attack3"))
		{
			result = State.ATTACK;
		}
        else if (currentStateHash == Animator.StringToHash ("Base.throw"))
		{
			result = State.THROW;
		}
        else if (currentStateHash == Animator.StringToHash ("Base.shoot"))
		{
			result = State.SHOOT;
		}
        else if (currentStateHash == Animator.StringToHash ("Base.counter"))
		{
			result = State.COUNTER;
		}
        else if (currentStateHash == Animator.StringToHash ("Base.wallon"))
		{
			result = State.WALLON;
		}
        else if (currentStateHash == Animator.StringToHash ("Base.idle"))
		{
			result = State.IDLE;
		}

		return result;
    }

    public void Shoot()
    {
        anim.SetTrigger("shoot");
        idleTimer = 0;
    }

    public void Throw()
    {
        anim.SetTrigger("throw");
        idleTimer = 0;
    }

    public void Counter()
    {
        anim.SetTrigger("counter");
        idleTimer = 0;
    }

    public void StopCounter()
    {
        anim.SetTrigger("normal");
        idleTimer = 0;
    }

    public void WallOn()
    {
        anim.SetTrigger("wallon");
        idleTimer = 0;
    }

    public void WallOf()
    {
        Normal();
        idleTimer = 0;
    }

    public void Normal()
    {
        anim.SetTrigger("normal");
        idleTimer = 0;
    }

    public void Attack()
    {
        anim.SetTrigger("attack");
        idleTimer = 0;
    }

    public void StopDash()
    {
        anim.SetTrigger("normal");
        idleTimer = 0;
    }

    public void Move(float speed)
    {
        anim.SetFloat("speed", speed);

		if (speed > 0.1)
		{
			idleTimer = 0;
		}
    }

	public void ChangeShootAnimationSpeed (float shootSpeed)
	{
		anim.SetFloat ("shoot_speed", shootSpeed);
	}
}
