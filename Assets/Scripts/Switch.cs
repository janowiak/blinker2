﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Switch : MonoBehaviour {

	[SerializeField] InteractionText interactionText;

    private bool playerInside = false;
    public SwitchAction action;
	
	// Update is called once per frame
	void Update ()
	{
		if (playerInside && Input.GetKeyDown (KeyManager.Instance.GetKeyForGameAction (KeyManager.GameAction.INTERACTION)))
		{
			action.Action();
		}
	}

    private void OnTriggerEnter(Collider other)
    {
		if (other.tag == GlobalConst.PLAYER_TAG)
		{
			playerInside = true;
			interactionText.Show ("Press " + action.GetKeyForAction () + " to " + action.GetActionName ());
		}
    }

    private void OnTriggerExit(Collider other)
    {
		if (other.tag == GlobalConst.PLAYER_TAG)
		{
			playerInside = false;
			interactionText.Hide ();
		}
    }
}
