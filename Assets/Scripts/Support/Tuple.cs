﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tuple <T, U>
{
    T item1;
    U item2;

    public Tuple (T item1, U item2)
    {
        this.item1 = item1;
        this.item2 = item2;
    }

    public T Item1
    {
        get { return item1; }
        set { item1 = value; }
    }

    public U Item2
    {
        get { return item2; }
        set { item2 = value; }
    }
}
