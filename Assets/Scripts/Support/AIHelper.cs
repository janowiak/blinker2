﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class AIHelper
{
    public static List <Tuple <Transform, float>> GetNClosest (List <Transform> list, int n, Vector3 position)
    {
        float d = 0;
        int indexOfFarest = 0;
        List<Tuple<Transform, float>> result = new List<Tuple<Transform, float>> ();

        for (int i = 0; i < list.Count; i++)
        {
            d = Vector3.Distance (list [i].gameObject.transform.position, position);

            if (result.Count < n)
            {
                result.Add (new Tuple<Transform, float> (list [i].transform, d));
            }
            else
            {
                indexOfFarest = GetBiggestInTupleIndex (result);
                result [indexOfFarest] = new Tuple<Transform, float> (list [i].transform, d);
            }
        }

        return result;
    }

    public static int GetBiggestInTupleIndex(List<Tuple<Transform, float>> list)
    {
        int result = 0;
        float maxD = int.MinValue;

        for (int i = 0; i < list.Count; i++)
        {
            if (list [i].Item2 > maxD)
            {
                maxD = list [i].Item2;
                result = i;
            }
        }

        return result;
    }

    public static List <Tuple<Transform, float>> GetNClosestToColliders (List<Collider> list, int n, Vector3 position)
    {
        float d = 0;
        int indexOfFarest = 0;
        Vector3 closestPoint = Vector3.zero;
        List<Tuple<Transform, float>> result = new List<Tuple<Transform, float>> ();

        for (int i = 0; i < list.Count; i++)
        {
            closestPoint = list [i].ClosestPointOnBounds (position);
            d = Vector3.Distance (list [i].gameObject.transform.position, position);

            if (result.Count < n)
            {
                result.Add (new Tuple<Transform, float> (list [i].transform, d));
            }
            else
            {
                indexOfFarest = GetBiggestInTupleIndex (result);
                result [indexOfFarest] = new Tuple<Transform, float> (list [i].transform, d);
            }
        }

        return result;
    }
}
