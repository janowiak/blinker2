﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class ChangeUIElementColor : ITweenAttachment
{
	public const string NAME = "ChangeUIElementColor";
	public const string ON_UPDATE_NAME = "ChangeUIElementColorOnUpdate";

	Graphic graphic;

	void Awake ()
	{
		graphic = GetComponent <Graphic> ();
	}

	public void ChangeUIElementColorOnUpdate (Color color)
	{
		if (graphic != null)
		{
			graphic.color = color;
		}
	}

	public override string GetName ()
	{
		return NAME;
	}

	public virtual string GetOnUpdateName ()
	{
		return ON_UPDATE_NAME;
	}
}

