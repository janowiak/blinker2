﻿using System;
using UnityEngine;


public class ChangeCanvasGroupAlpha : ITweenAttachment
{
	public const string NAME = "ChangeCanvasGroupAlpha";
	public const string ON_UPDATE_NAME = "ChangeCanvasGroupAlphaOnUpdate";

	CanvasGroup canvasGroup;

	void Awake ()
	{
		canvasGroup = GetComponent <CanvasGroup> ();
	}

	public void ChangeCanvasGroupAlphaOnUpdate (float alpha)
	{
		if (canvasGroup != null)
		{
			canvasGroup.alpha = alpha;
		}
	}

	public override string GetName ()
	{
		return NAME;
	}

	public virtual string GetOnUpdateName ()
	{
		return ON_UPDATE_NAME;
	}
}

