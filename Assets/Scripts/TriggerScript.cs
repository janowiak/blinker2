﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerScript : MonoBehaviour {

	[SerializeField] SpriteRenderer textBackground;
	[SerializeField] TextMesh text;
	[SerializeField] GameObject player;
	[SerializeField] Type type = Type.NONE;

	public delegate void PlayerTriggerEventHandler (TriggerScript trigger);
	public event PlayerTriggerEventHandler OnPlayerEnteredTrigger;
	public event PlayerTriggerEventHandler OnPlayerExitedTrigger;

    static Transform lastTransform;
	const float FADE_DURATION = 1f;

    public enum Type { ENABLE_BLINK, ENABLE_THROW, ENABLE_SHOOT, DISABLE_THROW, ENABLE_COUNTER, NONE}

	public Type TriggerType
	{
		get { return type;}
	}

	void Start ()
	{
		ImageProcessing.ChangeTextMeshAlpha (text, 0f);
		ImageProcessing.ChangeSpriteRendererAlpha (textBackground, 0f);
	}

	public static Transform LastTransform
	{
		get { return lastTransform; }
	}

    void OnTriggerEnter (Collider other)
    {
		if (other.tag == GlobalConst.PLAYER_TAG)
        {
            lastTransform = this.transform;

			if (OnPlayerEnteredTrigger != null)
			{
				OnPlayerEnteredTrigger (this);
			}
        } 
    }

    void OnTriggerExit (Collider other)
    {
		if (other.tag == GlobalConst.PLAYER_TAG)
        {
			if (OnPlayerExitedTrigger != null)
			{
				OnPlayerExitedTrigger (this);
			}
        } 
    }

	public void ShowText ()
	{
		iTween.Stop (this.gameObject);

		float time = (1f - text.color.a) * FADE_DURATION;
		Hashtable parameters = new Hashtable ();
		parameters.Add (ITweenHelper.From, text.color.a);
		parameters.Add (ITweenHelper.To, 1f);
		parameters.Add (ITweenHelper.Time, time);
		parameters.Add (ITweenHelper.OnUpdate, "OnTextAlphaUpdate");
		parameters.Add (ITweenHelper.OnUpdateTarget, this.gameObject);

		iTween.ValueTo (this.gameObject, parameters);
	}

	public void HideText ()
	{
		iTween.Stop (this.gameObject);

		float time = text.color.a * FADE_DURATION;
		Hashtable parameters = new Hashtable ();
		parameters.Add (ITweenHelper.From, text.color.a);
		parameters.Add (ITweenHelper.To, 0f);
		parameters.Add (ITweenHelper.Time, time);
		parameters.Add (ITweenHelper.OnUpdate, "OnTextAlphaUpdate");
		parameters.Add (ITweenHelper.OnUpdateTarget, this.gameObject);

		iTween.ValueTo (this.gameObject, parameters);
	}

	void OnTextAlphaUpdate (float value)
	{
		ImageProcessing.ChangeTextMeshAlpha (text, value);
		ImageProcessing.ChangeSpriteRendererAlpha (textBackground, value);
	}

	public void SetText (string text)
	{
		this.text.text = text;
	}
}
