﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

	//Normal bullet
	public const float DEFAULT_FIRE_RATE = 0.4f;
	public const int PLAYER_NORMAL_BULLET_SPEED = 300;
	public static Vector3 PLAYER_NORMAL_BULLET_SCALE = new Vector3 (1f, 1f, 3f);
	public static Color PLAYER_NORMAL_BULLET_COLOR = new Color (0.5f, 1.4f, 0.4f);
	public const int PLAYER_NORMAL_BULLET_DAMAGE = 40;

	//Enemy bullet
	public const int ENEMY_BULLET_SPEED = 70;
	public const int ENEMY_BULLET_DAMAGE = 10;

	//Fire rate up
	public const float SPEED_UP_FIRE_RATE = 0.2f;
	public static Color FIRE_RATE_POWER_UP_COLOR = Color.gray;

	//Spread
	public const int SPREAD_SHOT_BULLETS_COUNT = 20;
	public const int PLAYER_SPREAD_BULLET_SPEED = 100;
	public static Vector3 PLAYER_SPREAD_BULLET_SCALE = new Vector3 (0.2f, 0.2f, 2f);
	public static Color PLAYER_SPREAD_BULLET_COLOR = new Color (1f, 0f, 0.2f);
	public const int PLAYER_SPREAD_BULLET_DAMAGE = 5;
	public const float SPREAD_SHOT_ROTATION_RANGE = 10f;
	public const float SPREAD_SHOT_POSITION_RANGE = 0.5f;

	//Auto aim
	public const int PLAYER_AUTO_AIM_BULLET_SPEED = 90;
	public static Vector3 PLAYER_AUTO_AIM_BULLET_SCALE = new Vector3 (0.3f, 0.3f, 3f);
	public static Color PLAYER_AUTO_AIM_BULLET_COLOR = new Color (0.5f, 0f, 0.5f);
	public const int PLAYER_AUTO_AIM_BULLET_DAMAGE = 15;
	public const int AUTO_AIM_POINTER_DISTANCE = 30; //Distance from camera in gunsight direction from where closest enemy will be searched
	public const float AUTO_AIM_DEFAULT_ROTATION_SPEED = 2f;
	public const float AUTO_AIM_DELAY = 0.05f;

    public enum BulletType
    {
        PLAYER, ENEMY
    }

	public enum ShootType
	{
		NORMAL, SPREAD, AUTO_AIM
	}

	public delegate void OnCollisionHandler (GameObject sender, GameObject collider, Collision collision);
    public event OnCollisionHandler OnCollision;

    [SerializeField] GameObject playerBulletModel;
    [SerializeField] GameObject enemyBulletModel;
	[SerializeField] new Rigidbody rb;
	[SerializeField] ParticleSystem hitParticle;

    BulletType type;
	ShootType shootType;
    int damage;
    int speed;
	static System.Random rand =  new System.Random ();
	new Renderer renderer;

	Transform target = null;

	void Start ()
	{
		renderer = GetComponent <Renderer> ();
	}

    public BulletType Type
    {
        get { return type; }
    }
		
    public int Damage
    {
        get { return damage; }
    }

	public void Shoot (ShootType shootType, BulletType bulletType, Transform spawnPoint, Vector3 initialSpeed, bool randomize = false)
    {
        gameObject.SetActive (true);
		rb.isKinematic = false;
		this.type = bulletType;
		target = null;
		this.shootType = shootType;

        switch (bulletType)
        {
			case BulletType.ENEMY:
				playerBulletModel.SetActive (false);
				enemyBulletModel.SetActive (true);
				damage = ENEMY_BULLET_DAMAGE;
				speed = ENEMY_BULLET_SPEED;
				transform.tag = GlobalConst.ENEMY_BULLET_TAG;
				gameObject.layer = LayerMask.NameToLayer (GlobalConst.ENEMY_BULLET_LAYER);

                break;

		case BulletType.PLAYER:
			playerBulletModel.SetActive (true);
			enemyBulletModel.SetActive (false);
			speed = PLAYER_SPREAD_BULLET_SPEED;
			Material mat = playerBulletModel.GetComponent <Renderer> ().material;

			switch (shootType)
			{
			case ShootType.NORMAL:
				damage = PLAYER_NORMAL_BULLET_DAMAGE;
				playerBulletModel.transform.localScale = PLAYER_NORMAL_BULLET_SCALE;
				speed = PLAYER_NORMAL_BULLET_SPEED;
				mat.SetColor ("_EmissionColor", PLAYER_NORMAL_BULLET_COLOR);

				break;

			case ShootType.SPREAD:
				damage = PLAYER_SPREAD_BULLET_DAMAGE;
				playerBulletModel.transform.localScale = PLAYER_SPREAD_BULLET_SCALE;
				speed = PLAYER_SPREAD_BULLET_SPEED;
				mat.SetColor ("_EmissionColor", PLAYER_SPREAD_BULLET_COLOR);

				break;

			case ShootType.AUTO_AIM:
				damage = PLAYER_AUTO_AIM_BULLET_DAMAGE;
				playerBulletModel.transform.localScale = PLAYER_AUTO_AIM_BULLET_SCALE;
				speed = PLAYER_AUTO_AIM_BULLET_SPEED;
				mat.SetColor ("_EmissionColor", PLAYER_AUTO_AIM_BULLET_COLOR);
				StartCoroutine (autoAimTimer ());

				break;
			}
				
			transform.tag = GlobalConst.PLAYER_BULLET_TAG;
			gameObject.layer = LayerMask.NameToLayer (GlobalConst.PLAYER_BULLET_LAYER);
				
                break;
        }

        transform.position = spawnPoint.position;
        transform.rotation = spawnPoint.rotation;

		if (randomize)
		{
			transform.Rotate (new Vector3 ((float) (rand.NextDouble () * SPREAD_SHOT_ROTATION_RANGE - SPREAD_SHOT_ROTATION_RANGE / 2f),
				(float)(rand.NextDouble () * SPREAD_SHOT_ROTATION_RANGE - SPREAD_SHOT_ROTATION_RANGE / 2f), 0));
			transform.localPosition += new Vector3 ((float) (rand.NextDouble () * SPREAD_SHOT_POSITION_RANGE - SPREAD_SHOT_POSITION_RANGE / 2f),
				(float)(rand.NextDouble () * SPREAD_SHOT_POSITION_RANGE - SPREAD_SHOT_POSITION_RANGE / 2f), 0);
		}

        rb.velocity = transform.forward * speed;
    }

    void OnCollisionEnter(Collision collision)
    {
        if (OnCollision != null)
        {
			hitParticle.Play ();
            OnCollision (gameObject, collision.collider.gameObject, collision);

        }
    }

    public void Disable()
    {
        gameObject.SetActive (false);
    }

	IEnumerator autoAimTimer ()
	{
		yield return new WaitForSeconds (AUTO_AIM_DELAY);
		findTarget ();
	}

	void findTarget ()
	{
		target = GameplayController.Instance.GetClosestEnemy (transform.position + transform.forward * AUTO_AIM_POINTER_DISTANCE);
		rb.isKinematic = true;
	}
		
	void FixedUpdate ()
	{
		if (shootType == ShootType.AUTO_AIM && target != null)
		{
			float d = Vector3.Distance (transform.position, target.position);
			float rotationSpeed = AUTO_AIM_DEFAULT_ROTATION_SPEED / d; 

			Vector3 direction = target.position - transform.position;
			direction.Normalize();
			Vector3 directionDiff = direction - transform.forward;
			directionDiff *= rotationSpeed;
			transform.forward += directionDiff;

			Vector3 deltaPosition = transform.forward * Time.deltaTime * PLAYER_AUTO_AIM_BULLET_SPEED;
			rb.MovePosition (transform.position + deltaPosition);
		}
	}
}
