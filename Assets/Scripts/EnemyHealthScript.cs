﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealthScript : HealthScript
{
	[SerializeField] new Rigidbody rigidbody;
	[SerializeField] MaskHealth mask;
	[SerializeField] GameObject eyeLeft;
	[SerializeField] GameObject eyeRight;

	public const float DISSAPPEAR_MASS = 0.01f;
	public const float DEAD_MASS = 1f;
	public const float DEFAULT_MASS = 10f;
	public const float DEAD_DRAG = 0.5f;
	public const float DEFAULT_DRAG = 100f;
	public const float TIME_TO_RESET = 5f;

    // Use this for initialization
    void Start () 
	{
        Init();
	}
	
	public void SetEyesActive (bool active)
	{
		eyeLeft.SetActive (active);
		eyeRight.SetActive (active);
	}

	public override void DealDamage (int damage, Vector3 hitPosition)
	{
		if (! Dead)
		{
			blinkOnHit ();
		}

		base.DealDamage (damage, hitPosition);
	}

	public override void Death (Vector3 hitPosition)
    {
		base.Death (hitPosition);

		dead = true;

		rigidbody.isKinematic = false;
		rigidbody.useGravity = true;
		rigidbody.drag = DEAD_DRAG;
		rigidbody.mass = DEAD_MASS;
        GetComponent<EnemyScript> ().SetActive (false);

		SetEyesActive (false);

        if(! mask.Dead)
        {
			mask.Death (hitPosition);
        }

		StartCoroutine (disappearTimer ());
    }

	public override bool IsActive ()
	{
        return GetComponent<EnemyScript> ().IsActive ();
	}

	IEnumerator disappearTimer ()
	{
		yield return new WaitForSeconds (GlobalConst.ENEMY_DISAPPEAR_TIME);

		disappear ();

		yield return new WaitForSeconds (TIME_TO_RESET);

		Reset ();
	}

	public void Reset ()
	{
		StopCoroutine (disappearTimer ());

		rigidbody.isKinematic = true;
		rigidbody.useGravity = false;
		rigidbody.drag = DEFAULT_DRAG;
		rigidbody.mass = DEFAULT_MASS;
		dead = false;

		SetEyesActive (true);
		mask.Reset (transform);
		this.gameObject.SetActive (false);

		BoxCollider [] colliders = GetComponents <BoxCollider> ();

		for (int i = 0; i < colliders.Length; i ++)
		{
			colliders [i].isTrigger = false;
		}

		currentHealth = MaxHealth;
	}

	void disappear ()
	{
		rigidbody.mass = DISSAPPEAR_MASS;
		BoxCollider [] colliders = GetComponents <BoxCollider> ();

		for (int i = 0; i < colliders.Length; i ++)
		{
			colliders [i].isTrigger = true;
		}
	}
}
