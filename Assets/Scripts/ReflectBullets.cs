﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReflectBullets : MonoBehaviour {

	[SerializeField] CounteringScript counteringScript;
	[SerializeField] new Transform camera;
	
    void OnTriggerEnter (Collider other)
    {
		if (other.tag == GlobalConst.ENEMY_BULLET_TAG && counteringScript.CanCounter())
        {
            Rigidbody rb = other.gameObject.GetComponent<Rigidbody>();
            float speed = rb.velocity.magnitude;
            rb.velocity = (camera.forward + new Vector3(0, 0.1f, 0)) * speed;
        }
    }
}
