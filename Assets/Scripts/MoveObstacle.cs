﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveObstacle : MonoBehaviour {

    [SerializeField] GameObject pathContainer;
	[SerializeField] float time = 10f;

    List<Transform> pathPoints = new List<Transform> ();

    void Start ()
    {
        for (int i = 0; i < pathContainer.transform.childCount; i++)
        {
            pathPoints.Add (pathContainer.transform.GetChild (i));
        }

        StartMoving ();
    }

    public void StartMoving ()
    {
        Hashtable iTweenHash = new Hashtable ();
        Vector3 [] path = new Vector3 [pathPoints.Count];

        for (int i = 0; i < pathPoints.Count; i++)
        {
            path [i] = pathPoints [i].position;
        }

		iTweenHash.Add (ITweenHelper.Path, path);
		iTweenHash.Add (ITweenHelper.Time, time);
		iTweenHash.Add (ITweenHelper.OnComplete, "OnMovingComplete");
		iTweenHash.Add (ITweenHelper.EaseType, iTween.EaseType.easeInOutQuad);

        iTween.MoveTo (gameObject, iTweenHash);
    }

    void OnMovingComplete ()
    {
        iTween.Stop (gameObject);
        StartMoving ();
    }
}
