﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalConst {

	public static Vector3 INVALID_VECTOR = new Vector3 (int.MinValue, int.MinValue, int.MinValue);
	public const int INVALID_VALUE = -1;

    //Tags
    public const string NPC_TAG = "NPC";
    public const string PLAYER_TAG = "Player";
    public const string PLAYER_FACE_TAG = "PlayerFace";
    public const string WALL_TAG = "Wall";
    public const string DAMAGE_SPHERE_TAG = "DamageSphere";
    public const string PLAYER_BULLET_TAG = "PlayerBullet";
	public const string ENEMY_BULLET_TAG = "EnemyBullet";
	public const string ENEMY_BULLET_LAYER = "EnemyBullet";
	public const string PLAYER_BULLET_LAYER = "PlayerBullet";
	public const string ENVIRONMENT_TAG = "Environment";
	public const string KATANA_TAG = "Katana";
	public const string MAIN_CAMERA_TAG = "MainCamera";
	public const string BLINK_DESTINATION_TAG = "BlinkDestination";

	public const int DEFAULT_MOVEMENT_SPEED = 25;
	public const int DEFAULT_JUMP_POWER = 15000;

	public const float DEFAULT_BLINK_REFRESH_RATE =  0.002f;
	public const float POWER_UP_BLINK_REFRESH_RATE = 0.01f;
	public static Color BLINK_POWER_UP_COLOR = Color.blue;

	public const int ENEMY_DISAPPEAR_TIME = 15;
	public const int ENEMY_DEATH_POINTS = 10;
	public const int ENEMY_HIT_POINTS = 1;
}
