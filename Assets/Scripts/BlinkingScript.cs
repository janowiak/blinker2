﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class BlinkingScript : MonoBehaviour, Controls {

	[SerializeField] GameObject playerPhantom; //Object left after blinking, for dodge-slowmo use
	[SerializeField] float blinkSpeed = 100f;
	[SerializeField] float blinkLength = 20f;
	[SerializeField] float distToObstacleThreshold = 6f; //When distance to obstacle is smaller blinkig is canceled
	[SerializeField] bool active = true;
	[SerializeField] GameObject blinkDestination;
	[SerializeField] Image blinkSlider; //GUI element representing blink bar

	float blinkRefreshRate = GlobalConst.DEFAULT_BLINK_REFRESH_RATE;
    RigAnimationManager animManager;
    Rigidbody rb;

    ShootingScript shootingScript;
    ThrowingScript throwingScript;
    private SlowMotion slowMotion;

    float currentBlinkgSpeed;
    bool blinking = false;
    bool throwBlink = false;

    // Use this for initialization
    void Start () 
	{
        currentBlinkgSpeed = blinkSpeed;
        animManager = GetComponent <RigAnimationManager>();
        shootingScript = GetComponent <ShootingScript>();
        throwingScript = GetComponent <ThrowingScript>();
        slowMotion = GetComponent <SlowMotion>();

        rb = GetComponent<Rigidbody>();
        //blinkDestination = GameObject.Find("BlinkDestination");
        //blinkSlider = GameObject.Find("BlinkSlider").GetComponent<Image>();
    }

	public void SetBlinkRefreshRate (float refreshRate)
	{
		this.blinkRefreshRate = refreshRate;
	}

	public bool Active
	{
		get { return active; }
		set { active = value; }
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (active)
		{
			if (Input.GetKeyDown (KeyManager.Instance.GetKeyForGameAction (KeyManager.GameAction.BLINK)) && ! blinking)
			{
				blink();
			}
				
			//Move towards blink direction
			if (blinking)
			{
				transform.position = Vector3.MoveTowards(transform.position,
					blinkDestination.transform.position, currentBlinkgSpeed * Time.deltaTime);
			}
				
			//Refill blink slider
			if (blinkSlider.fillAmount < 1)
			{
				blinkSlider.fillAmount += blinkRefreshRate;
			}
		}
    }

    public bool IsBlinking()
    {
        return blinking;
    }

    public void BlinkToKatana(Transform katanaTransform)
    {
        currentBlinkgSpeed = 2 * blinkSpeed;
        blinkDestination.transform.position = katanaTransform.position + transform.up * 5 + katanaTransform.forward * 7;
        blinking = true;
        rb.useGravity = false;
        throwBlink = true;
    }

    void blink()
    {
        currentBlinkgSpeed = blinkSpeed;
        float horMove = Input.GetAxisRaw("Horizontal");
        float verMove = Input.GetAxisRaw("Vertical");

        //No movement - no blink
		if (horMove == 0 && verMove == 0)
		{
			return;
		}

        if (blinkSlider.fillAmount < 0.33f)
        {
            blinkSlider.GetComponentInParent<BlinkImage>().BlinkRed();
            return;
        }

        blinkSlider.fillAmount -= 0.33f;

        //Attack animation
		if (verMove >= 0.2 && !throwingScript.IsThrown () && animManager.GetState () != RigAnimationManager.State.COUNTER)
		{
			animManager.Attack();
		}

        blinking = true;
        rb.useGravity = false;
        Vector3 blinkDirection = transform.rotation * (new Vector3(horMove, 0, verMove).normalized);
        blinkDestination.transform.position = transform.position + blinkDirection * blinkLength;

        playerPhantom.transform.position = transform.position;

        slowMotion.EnableSlowMotion(); //Slowmotion when dodge succesfull enabled

        //Check if there are obstacles on blink path
        RaycastHit raycastHit;
        Vector3 rayDirection = blinkDestination.transform.position - transform.position;
        float rayLength = Vector3.Distance(transform.position, blinkDestination.transform.position);
        Ray ray = new Ray(transform.position, rayDirection);

        if (Physics.Raycast(ray, out raycastHit, rayLength))
        {
			if (raycastHit.collider.tag == GlobalConst.WALL_TAG)
            {
                blinkDestination.transform.position = raycastHit.point;
                float distToObstacle = Vector3.Distance(transform.position, blinkDestination.transform.position);

				if (distToObstacle < distToObstacleThreshold)
				{
					StopBlinking ();
				}
                   
            }
        }
    }


    public void StopBlinking()
    {
        blinking = false;
        rb.useGravity = true;

        if (throwingScript.IsThrown() && throwBlink)
        {
            throwingScript.PickUp();
            throwBlink = false;
        }
    }

    void OnTriggerEnter(Collider other)
    {
		if (other.gameObject.tag == GlobalConst.BLINK_DESTINATION_TAG && blinking)
		{
			StopBlinking ();
		}
    }

    void OnCollisionEnter(Collision other)
    {
		if ((other.gameObject.tag == GlobalConst.WALL_TAG) && blinking)
		{
			StopBlinking ();
		}
    }

    public bool IsActive()
    {
        return active;
    }

    public void SetActive(bool active)
    {
        this.active = active;
    }
}
