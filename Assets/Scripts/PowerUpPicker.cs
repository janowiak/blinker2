﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpPicker : MonoBehaviour {

	[SerializeField] PlayerHealth health;
	[SerializeField] ShootingScript shootingScript;
	[SerializeField] BloodOverlayScript bloodOverlay;
	[SerializeField] FPSMovementScript movement;
	[SerializeField] BlinkingScript blinking;
	[SerializeField] RigAnimationManager animManager;

	[SerializeField] PowerUpUI fireRatePowerUp;
	[SerializeField] PowerUpUI bulletTypePowerUp;
	[SerializeField] PowerUpUI jumpPowerUp;
	[SerializeField] PowerUpUI staminaPowerUp;

	public void PickUpPowerUp (PowerUp.PowerUpType powerUpType)
	{
		switch (powerUpType)
		{
		case PowerUp.PowerUpType.SMALL_HEALTH:
			pickUpSmallHealthPack ();

			break;

		case PowerUp.PowerUpType.SPREAD_SHOT:
			pickUpSpreadShot ();

			break;

		case PowerUp.PowerUpType.FIRE_RATE:
			pickUpSpeedShootUp ();

			break;

		case PowerUp.PowerUpType.AUTO_AIM:
			pickUpAutoAimShot ();

			break;

		case PowerUp.PowerUpType.MOVEMENT:
			pickUpMovementPowerUp ();

			break;

		case PowerUp.PowerUpType.STAMINA:
			pickUpStaminaPowerUp ();

			break;
		}
	}

	void pickUpStaminaPowerUp ()
	{
		blinking.SetBlinkRefreshRate (GlobalConst.POWER_UP_BLINK_REFRESH_RATE);
		bloodOverlay.BlinkPurple ();
		staminaPowerUp.gameObject.SetActive (true);
		StartCoroutine (staminaTimer ());
		staminaPowerUp.SetColor (GlobalConst.BLINK_POWER_UP_COLOR);
	}

	void setNormalStamina ()
	{
		blinking.SetBlinkRefreshRate (GlobalConst.DEFAULT_BLINK_REFRESH_RATE);
		staminaPowerUp.gameObject.SetActive (false);
		staminaPowerUp.FillSlider (1f);
	}

	void pickUpMovementPowerUp ()
	{
		movement.SetJumpPowerUp (PowerUp.POWER_UP_JUMP_POWER);
		movement.SetMovementSpeed (PowerUp.POWER_UP_MOVEMENT_SPEED);
		bloodOverlay.BlinkPurple ();
		jumpPowerUp.gameObject.SetActive (true);
		StartCoroutine (movementTimer ());
		jumpPowerUp.SetColor (PowerUp.MOVEMENT_POWER_UP_COLOR);
	}

	void setNormalMovement ()
	{
		movement.SetJumpPowerUp (GlobalConst.DEFAULT_JUMP_POWER);
		movement.SetMovementSpeed (GlobalConst.DEFAULT_MOVEMENT_SPEED);
		jumpPowerUp.gameObject.SetActive (false);
		jumpPowerUp.FillSlider (1f);
	}

	void pickUpSmallHealthPack ()
	{
		health.DealDamage (- PowerUp.HEALTH_PACK_HEAL_AMOUNT, GlobalConst.INVALID_VECTOR);
	}

	void pickUpSpreadShot ()
	{
		StopCoroutine (bulletTypeTimer ());
		shootingScript.ShootType = Bullet.ShootType.SPREAD;
		StartCoroutine (bulletTypeTimer ());
		bloodOverlay.BlinkPurple ();
		bulletTypePowerUp.gameObject.SetActive (true);
		bulletTypePowerUp.SetColor (Bullet.PLAYER_SPREAD_BULLET_COLOR);
		bulletTypePowerUp.SetText ("SPREAD SHOT");
	}

	void pickUpAutoAimShot ()
	{
		StopCoroutine (bulletTypeTimer ());
		shootingScript.ShootType = Bullet.ShootType.AUTO_AIM;
		StartCoroutine (bulletTypeTimer ());
		bloodOverlay.BlinkPurple ();
		bulletTypePowerUp.gameObject.SetActive (true);
		bulletTypePowerUp.SetColor (Bullet.PLAYER_AUTO_AIM_BULLET_COLOR);
		bulletTypePowerUp.SetText ("AUTO AIM");
	}

	void pickUpSpeedShootUp ()
	{
		shootingScript.FireRate = Bullet.SPEED_UP_FIRE_RATE;
		StartCoroutine (fireRateUpTimer ());
		bloodOverlay.BlinkPurple ();
		fireRatePowerUp.gameObject.SetActive (true);
		fireRatePowerUp.SetColor (Bullet.FIRE_RATE_POWER_UP_COLOR);
		animManager.ChangeShootAnimationSpeed (Bullet.DEFAULT_FIRE_RATE /  Bullet.SPEED_UP_FIRE_RATE);
	}

	void setNormalShot ()
	{
		shootingScript.ShootType = Bullet.ShootType.NORMAL;
		bulletTypePowerUp.FillSlider (1f);
		bulletTypePowerUp.gameObject.SetActive (false);
	}

	void setNormalFireRate ()
	{
		shootingScript.FireRate = Bullet.DEFAULT_FIRE_RATE;
		fireRatePowerUp.FillSlider (1f);
		fireRatePowerUp.gameObject.SetActive (false);
		animManager.ChangeShootAnimationSpeed (1f);
	}

	IEnumerator bulletTypeTimer ()
	{
		float timer = 0;

		while (timer < PowerUp.BONUS_SHOOT_TYPE_DURATION)
		{
			timer += Time.deltaTime;
			bulletTypePowerUp.FillSlider (1f - (timer / PowerUp.BONUS_SHOOT_TYPE_DURATION));
			yield return new WaitForEndOfFrame ();
		}

		setNormalShot ();
	}

	IEnumerator staminaTimer ()
	{
		float timer = 0;

		while (timer < PowerUp.BONUS_SHOOT_TYPE_DURATION)
		{
			timer += Time.deltaTime;
			staminaPowerUp.FillSlider (1f - (timer / PowerUp.BONUS_SHOOT_TYPE_DURATION));
			yield return new WaitForEndOfFrame ();
		}

		setNormalStamina ();
	}

	IEnumerator movementTimer ()
	{
		float timer = 0;

		while (timer < PowerUp.BONUS_SHOOT_TYPE_DURATION)
		{
			timer += Time.deltaTime;
			jumpPowerUp.FillSlider (1f - (timer / PowerUp.BONUS_SHOOT_TYPE_DURATION));
			yield return new WaitForEndOfFrame ();
		}

		setNormalMovement ();
	}


	IEnumerator fireRateUpTimer ()
	{
		float timer = 0;

		while (timer < PowerUp.BONUS_SHOOT_TYPE_DURATION)
		{
			timer += Time.deltaTime;
			fireRatePowerUp.FillSlider (1f - (timer / PowerUp.BONUS_SHOOT_TYPE_DURATION));
			yield return new WaitForEndOfFrame ();
		}

		setNormalFireRate ();
	}
}
