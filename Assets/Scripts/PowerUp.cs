﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class PowerUp : MonoBehaviour {

	public const int POWER_UP_DISTANCE_THRESHOLD = 20;
	public const float POWER_UP_SPEED = 0.4f;
	public const int POWER_UP_PICK_UP_THRESHOLD = 5;
	public const int BONUS_SHOOT_TYPE_DURATION = 100;
	public const int HEALTH_PACK_HEAL_AMOUNT = 30;
	public const int POWER_UP_MOVEMENT_SPEED = 60;
	public const int POWER_UP_JUMP_POWER = 20000;
	public static Color MOVEMENT_POWER_UP_COLOR = Color.green;
	public static Color HEALTH_PACK_COLOR = new Color (0f, 1f, 0.58f);
	public static string EMISSION_COLOR = "_EmissionColor";

	const float DEFAULT_LIGHT_INTENSITY = 2f;
	const float DEFAULT_LIGHT_RANGE = 20f;
	const float SPAWN_ANIMATION_DURATION = 1.7f;
	static Vector3 DEFAULT_SCALE = Vector3.one * 5f;

	#if UNITY_EDITOR
	[CustomEditor (typeof (PowerUp))]
	public class customButton : Editor
	{
		public override void OnInspectorGUI ()
		{
			DrawDefaultInspector ();

			PowerUp myScript = (PowerUp) target;

			if (GUILayout.Button ("Validate"))
			{
				myScript.OnValidate ();
			}
		}
	}
	#endif

	public enum PowerUpType
	{
		SMALL_HEALTH, LARGE_HEALTH, SPREAD_SHOT, FIRE_RATE, AUTO_AIM, MOVEMENT, STAMINA
	}

	void OnValidate ()
	{
		Renderer renderer = GetComponent <Renderer> ();
		Material material = renderer.sharedMaterial;

		if (material != null)
		{
			switch (powerUpType)
			{
				case PowerUpType.AUTO_AIM:
					material.color = Bullet.PLAYER_AUTO_AIM_BULLET_COLOR;
					material.SetColor (EMISSION_COLOR, Bullet.PLAYER_AUTO_AIM_BULLET_COLOR);
					pointLight.color = Bullet.PLAYER_AUTO_AIM_BULLET_COLOR;
					break;

				case PowerUpType.MOVEMENT:
					material.color = PowerUp.MOVEMENT_POWER_UP_COLOR;
					material.SetColor (EMISSION_COLOR, PowerUp.MOVEMENT_POWER_UP_COLOR);
					pointLight.color = PowerUp.MOVEMENT_POWER_UP_COLOR;

					break;

				case PowerUpType.LARGE_HEALTH:
					material.color = PowerUp.HEALTH_PACK_COLOR;
					material.SetColor (EMISSION_COLOR, PowerUp.HEALTH_PACK_COLOR);
					pointLight.color = PowerUp.HEALTH_PACK_COLOR;

					break;

				case PowerUpType.SMALL_HEALTH:
					material.color = PowerUp.HEALTH_PACK_COLOR;
					material.SetColor (EMISSION_COLOR, PowerUp.HEALTH_PACK_COLOR);
					pointLight.color = PowerUp.HEALTH_PACK_COLOR;

					break;

				case PowerUpType.FIRE_RATE:
					material.color = Bullet.FIRE_RATE_POWER_UP_COLOR;
					material.SetColor (EMISSION_COLOR, Bullet.FIRE_RATE_POWER_UP_COLOR);
					pointLight.color = Bullet.FIRE_RATE_POWER_UP_COLOR;

					break;

				case PowerUpType.SPREAD_SHOT:
					material.color = Bullet.PLAYER_SPREAD_BULLET_COLOR;
					material.SetColor (EMISSION_COLOR, Bullet.PLAYER_SPREAD_BULLET_COLOR);
					pointLight.color = Bullet.PLAYER_SPREAD_BULLET_COLOR;

					break;

				case PowerUpType.STAMINA:
					material.color = GlobalConst.BLINK_POWER_UP_COLOR;
					material.SetColor (EMISSION_COLOR, GlobalConst.BLINK_POWER_UP_COLOR);
					pointLight.color = GlobalConst.BLINK_POWER_UP_COLOR;

					break;
			}
		}
	}

	[SerializeField] PowerUpType powerUpType;
	[SerializeField] PowerUpPicker player;
	[SerializeField] Light pointLight;

	bool stickedToPlayer = false;
	new Rigidbody rigidbody;
	new Collider collider;

	void Awake ()
	{
		rigidbody = GetComponent <Rigidbody> ();
		collider = GetComponent <Collider> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		float distanceToPlayer = Vector3.Distance (transform.position, player.transform.position);

		if (distanceToPlayer <= PowerUp.POWER_UP_DISTANCE_THRESHOLD && powerUpSpecificCondition ())
		{
			if (! stickedToPlayer)
			{
				stickedToPlayer = true;
				rigidbody.isKinematic = true;
				rigidbody.useGravity = false;
			}

			Vector3 direction = player.transform.position - transform.position;
			direction.Normalize ();
			transform.position += direction * PowerUp.POWER_UP_SPEED;

			if (distanceToPlayer <= PowerUp.POWER_UP_DISTANCE_THRESHOLD / 2f)
			{
				player.PickUpPowerUp (powerUpType);
				Destroy (gameObject);
			}

		}
		else if (stickedToPlayer)
		{
			stickedToPlayer = false;
			rigidbody.isKinematic = false;
		}
	}

	bool powerUpSpecificCondition ()
	{
		bool result = true;

		return result;
	}

	void Start ()
	{
		if (pointLight != null)
		{
			pointLight.intensity = DEFAULT_LIGHT_INTENSITY;
			pointLight.range = 0;
			this.transform.localScale = Vector3.zero;
			rigidbody.useGravity = false;
			collider.isTrigger = true;
		}

		Spawn ();
	}

	public void Spawn ()
	{
		Hashtable parameters = new Hashtable ();
		parameters.Add (ITweenHelper.From, 0f);
		parameters.Add (ITweenHelper.To, DEFAULT_LIGHT_RANGE);
		parameters.Add (ITweenHelper.OnUpdate, "OnLightRangeUpdate");
		parameters.Add (ITweenHelper.Time, SPAWN_ANIMATION_DURATION * 0.8f);
		parameters.Add (ITweenHelper.OnComplete, "OnLightRangeComplete");
		parameters.Add (ITweenHelper.OnUpdateTarget, this.gameObject);
		parameters.Add (ITweenHelper.OnCompleteTarget, this.gameObject);

		iTween.ValueTo (this.gameObject, parameters);
	}

	void OnLightIntensityUpdate (float value)
	{
		pointLight.intensity = value;
	}

	void OnLightIntensityComplete ()
	{
		
	}

	void OnLightRangeUpdate (float value)
	{
		pointLight.range = value;
	}

	void OnLightRangeComplete ()
	{
		collider.isTrigger = false;

		Hashtable parameters = new Hashtable ();
		parameters.Add (ITweenHelper.From, DEFAULT_LIGHT_INTENSITY);
		parameters.Add (ITweenHelper.To, 0);
		parameters.Add (ITweenHelper.OnUpdate, "OnLightIntensityUpdate");
		parameters.Add (ITweenHelper.Time, SPAWN_ANIMATION_DURATION * 0.2f);
		parameters.Add (ITweenHelper.OnComplete, "OnLightIntensityComplete");
		parameters.Add (ITweenHelper.OnUpdateTarget, this.gameObject);
		parameters.Add (ITweenHelper.OnCompleteTarget, this.gameObject);

		iTween.ValueTo (this.gameObject, parameters);

		parameters = ITweenHelper.getScaleParameters (DEFAULT_SCALE, SPAWN_ANIMATION_DURATION * 0.1f);
		iTween.ScaleTo (this.gameObject, parameters);
	}
}
