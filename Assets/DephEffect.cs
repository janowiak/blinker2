﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DephEffect : MonoBehaviour {

    public Shader curShader;
    public float ChromaticAbberation = 1.0f;
    private Material curMaterial;

    Material material
    {
        get
        {
            if (curMaterial == null)
            {
                curMaterial = new Material (curShader);
                curMaterial.hideFlags = HideFlags.HideAndDontSave;
            }
            return curMaterial;
        }
    }

    // Use this for initialization
    void Start () {
        if (!SystemInfo.supportsImageEffects)
        {
            enabled = false;
            return;
        }
    }

    void OnRenderImage(RenderTexture sourceTexture, RenderTexture destTexture)
    {
        if (curShader != null)
        {
            material.SetFloat ("_AberrationOffset", ChromaticAbberation);
            Graphics.Blit (sourceTexture, destTexture, material);
        }
        else
        {
            Graphics.Blit (sourceTexture, destTexture);
        }
    }

    void OnDisable()
    {
        if (curMaterial)
        {
            DestroyImmediate (curMaterial);
        }

    }

    // Update is called once per frame
    void Update () {
		
	}
}
