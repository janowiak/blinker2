﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "DepthEffect" { Properties{ _MainTex("Diffuse (RGB) Alpha (A)", 2D) = "white" {} }
SubShader
{ 
	//Tags{ "Queue" = "Transparent" }
	Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" }
	Blend SrcAlpha OneMinusSrcAlpha
	Pass{
	ZTest Always Cull Off ZWrite Off Fog{ Mode off }

CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#pragma fragmentoption ARB_precision_hint_fastest 
#include "UnityCG.cginc"
sampler2D _MainTex;
uniform float _AberrationOffset;

struct v2f
{
	float4 pos  : POSITION;
	float2 uv   : TEXCOORD0;
	float3 wpos : TEXCOORD1;
	float3 vpos : TEXCOORD2;
};

v2f vert(appdata_img v)
{
	v2f o;
	o.pos = UnityObjectToClipPos(v.vertex);
	float3 worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
	o.wpos = worldPos;
	o.vpos = v.vertex.xyz;
	o.uv = v.texcoord.xy;
	return o;
}

float4 frag(v2f i) : COLOR
{
	float4 r = tex2D(_MainTex, i.uv);

	r.a = 0.1;
	

	if (i.wpos.z > 10)
	{
		r.a = 0.1;
		r.b = 1;
	}
	else
	{
		r.a = 1;
		r.b = 0;
	}
	return r;
}
ENDCG
}
}
FallBack "Diffuse"
}