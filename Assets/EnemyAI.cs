﻿using Assets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour {

    [SerializeField] EnemySpawner enemySpawner;
    [SerializeField] ObstaclesManager obstaclesManager;
    [SerializeField] Transform player;
    [SerializeField]  Rigidbody rigidbody;

    List<Vector3 []> inputVectors = new List<Vector3 []> ();
    List<Vector3 []> outputVectors = new List<Vector3 []> ();
    bool gatheringData = false;

    const int OBSTACLES_COUNT = 4;

    NeuralNetwork neuralNetwork;

    private void Start()
    {
        neuralNetwork = new NeuralNetwork (OBSTACLES_COUNT + 1, 10, 3);
        neuralNetwork.SetWeights (neuralNetwork.GetRandomWeights ());
    }

    List <Tuple<Transform, float>> getClosestObstaclesTransforms ()
    {
        List<Tuple<Transform, float>> totalObstacles = new List<Tuple<Transform, float>> ();
        List<Tuple<Transform, float>> nClosestEnemies = new List<Tuple<Transform, float>> ();
        List<Tuple<Transform, float>> nClosestObstacles = new List<Tuple<Transform, float>> ();

        nClosestEnemies = enemySpawner.GetNColosestEnemies (this.transform.position, OBSTACLES_COUNT);
        nClosestObstacles = obstaclesManager.GetNClosestObstacles (this.transform.position, OBSTACLES_COUNT);

        for (int i = 0; i < nClosestEnemies.Count; i++)
        {
            totalObstacles.Add (nClosestEnemies [i]);
        }

        for (int i = 0; i < nClosestObstacles.Count; i++)
        {
            totalObstacles.Add (nClosestObstacles [i]);
        }

        totalObstacles.Sort ((x, y) => x.Item2.CompareTo (y.Item2));

        return totalObstacles;
    }

    public Vector3 [] GetInputVectors ()
    {
        Vector3 [] result = new Vector3 [OBSTACLES_COUNT + 1];
        Vector3 normalizationVector = new Vector3 (1f / EnemySpawner.SPAWN_RANGE_X, 
            1f / EnemySpawner.SPAWN_RANGE_X, 1f / EnemySpawner.SPAWN_RANGE_X);

        result [0] = getToPlayerDirectionVector ();
        result [0].Scale (normalizationVector);

        List<Tuple<Transform, float>> closestObstacles = getClosestObstaclesTransforms ();

        for (int i = 1; i < OBSTACLES_COUNT + 1; i ++)
        {
            result [i] = closestObstacles [i - 1].Item1.position - this.transform.position;
            result [i].Scale (normalizationVector);
        }

        return result;
    }

    Vector3 getToPlayerDirectionVector ()
    {
        Vector3 result = player.transform.position - this.transform.position;
        

        return result;
    }

    private void Update()
    {
        if (Input.GetKeyDown (KeyCode.G))
        {
            if (!gatheringData)
            {
                startGatheringData ();
                Debug.Log ("Start gathering data");
            }
            else
            {
                stopGatheringData ();
                Debug.Log ("Stop gathering data");

                saveData ();
            }
        }
    }

    private void FixedUpdate()
    {
        Vector3 [] inputVectors = GetInputVectors ();
        double [] input = new double [(OBSTACLES_COUNT + 1) * 3];
        int index = 0;

        for (int i = 0; i < inputVectors.Length; i ++)
        {
            index = i * 3;
            input [index] = inputVectors [i].x;
            input [index + 1] = inputVectors [i].y;
            input [index + 2] = inputVectors [i].z;
        }

        double [] output = neuralNetwork.GetOutput (input);

        float vertical = (float) output [0];
        float horizontal = (float)output [1];
        float mouseY = (float)output [2];

        Vector3 directionVector = new Vector3 (horizontal, mouseY, vertical);

        

        if (horizontal > 1)
        {
            horizontal = 1f;
        }
        else if (horizontal < - 1)
        {
            horizontal = -1f;
        }

        if (vertical > 1)
        {
            vertical = 1f;
        }
        else if (vertical < -1)
        {
            vertical = -1f;
        }

        if (mouseY > 1)
        {
            mouseY = 1f;
        }
        else if (mouseY < -1)
        {
            mouseY = -1f;
        }

        rigidbody.MovePosition (transform.position + directionVector);

        //Debug.Log ("V: " + vertical + ", H: " + horizontal + ", MY: " + mouseY);
    }

    void startGatheringData ()
    {
        gatheringData = true;
        StartCoroutine (gatherDataCycle ());
    }

    void stopGatheringData()
    {
        gatheringData = false;
    }

    IEnumerator gatherDataCycle ()
    {
        if (gatheringData)
        {
            Vector3 [] gatheredData = GetInputVectors ();
            inputVectors.Add (gatheredData);

            yield return new WaitForSeconds (1 / 33f);
        }

        onGatherDataCycleCompleted ();
    }

    void onGatherDataCycleCompleted ()
    {
        if (gatheringData)
        {
            StartCoroutine (gatherDataCycle ());
        }
    }

    void saveData ()
    {
        string line = "";

        using (System.IO.StreamWriter file =
            new System.IO.StreamWriter (@"WriteLines2.txt"))
        {
            for (int i = 0; i < inputVectors.Count; i ++)
            {
                line = "";

                for (int j = 0; j < inputVectors [i].Length; j ++)
                {
                    line += inputVectors [i] [j].x + " " + inputVectors [i] [j].y + " " + inputVectors [i] [j].z + " ";
                }

                file.WriteLine (line);
            }
        }
    }

}
