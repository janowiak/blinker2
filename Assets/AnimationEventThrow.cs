﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationEventThrow : MonoBehaviour {

	[SerializeField] ThrowingScript throwingScript;

	public void Throw ()
	{
		throwingScript.Throw ();
	}
}
